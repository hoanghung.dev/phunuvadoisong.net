<?php
namespace Soul\Mysql;

use \PDOException, \PDO;
use Soul\Exception;

class Driver extends PDO
{
    /**
     * @param $dsn
     * @param string $user
     * @param string $passwd
     */
    public function __construct($dsn, $user = "", $passwd = "")
    {
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );

        try {
            parent::__construct($dsn, $user, $passwd, $options);
            parent::exec("set names utf8");
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * @param $table
     * @param $where
     * @param string $bind
     */
    public function delete($table, $where, $bind = "")
    {
        $sql = "DELETE FROM " . $table . " WHERE " . $where . ";";
        return $this->prepare($sql)->execute($bind);
    }

    /**
     * @param $table
     * @param $info
     * @return array
     */
    private function filter($table, $info)
    {
        $driver = $this->getAttribute(PDO::ATTR_DRIVER_NAME);
        if ($driver == 'sqlite') {
            $sql = "PRAGMA table_info('" . $table . "');";
            $key = "name";
        } elseif ($driver == 'mysql') {
            $sql = "DESCRIBE `" . $table . "`;";
            $key = "Field";
        } else {
            $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = `$table`;";
            $key = "column_name";
        }

        $list = $this->query($sql)->fetchAll(PDO::FETCH_ASSOC);


        if (false !== $list) {
            $fields = array();
            foreach ($list as $record)
                $fields[] = $record[$key];
            return array_values(array_intersect($fields, array_keys($info)));
        }
        return array();
    }

    /**
     * @param $bind
     * @return array
     */
    private function cleanup($bind)
    {
        if (!is_array($bind)) {
            if (!empty($bind))
                $bind = array($bind);
            else
                $bind = array();
        }
        return $bind;
    }

    /**
     * @param $table
     * @param $info
     * @return bool
     */
    public function insert($table, $info)
    { 
        $bind = array();
        $sql = "INSERT INTO `" . $table . "`";
        if(array_key_exists(0, $info) == true){
            $fields = $this->filter($table, $info[0]);
            $sql .= "(" . implode($fields, ", ") . ") VALUES ";
            foreach ($info as $key=>$value){
                if($key != 0) $sql .= ',';
                $fields = $this->filter($table, $info[$key]);
                $sql .= "(:" . implode($fields, "_".$key.", :") . "_".$key.")";

                foreach ($fields as $field) {
                    $bind[":$field"."_"."$key"] = $value[$field];
                }
            }
        }else{
            $fields = $this->filter($table, $info);
            $sql .= "(" . implode($fields, ", ") . ") VALUES (:" . implode($fields, ", :") . ");";


            foreach ($fields as $field)
                $bind[":$field"] = $info[$field];
        }

        try {
            $this->prepare($sql)->execute($bind);
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param $table
     * @param $info
     * @param $where
     * @param string $bind
     * @return bool
     */
    public function update($table, $info, $where, $bind = "")
    {
        $fields = $this->filter($table, $info);
        $fieldSize = sizeof($fields);

        $sql = "UPDATE `" . $table . "` SET ";
        for ($f = 0; $f < $fieldSize; ++$f) {
            if ($f > 0)
                $sql .= ", ";
            $sql .= $fields[$f] . " = :update_" . $fields[$f];
        }
        $sql .= " WHERE " . $where . ";";

        $bind = $this->cleanup($bind);
        foreach ($fields as $field)
            $bind[":update_$field"] = $info[$field];

        return $this->prepare($sql)->execute($bind);
    }
}
