<?php
namespace Soul\Mvc;

use Soul\Registry;

abstract class Model
{
    /**
     * @var \Soul\Mysql\Driver $_mysql
     */
    protected $_mysql = null;
    /**
     * @var MongoClient $_mongodb
     */
    protected $_mongodb = null;
    /**
     * @var Redis $_redis
     */
    protected $_redis = null;
    /**
     * @var Memcache $_memcache
     */
    protected $_memcache = null;

    public function __construct()
    {
        $this->init();
    }

    /**
     * @param $args
     * @param string $defaults
     * @return array
     */
    public function parseArgs($args, $defaults = '')
    {
        if (is_object($args))
            $r = get_object_vars($args);
        elseif (is_array($args))
            $r = & $args;
        else
            self::parseStr($args, $r);

        if (is_array($defaults))
            return array_merge($defaults, $r);
        return $r;
    }

    /**
     *
     * @param string $string The string to be parsed.
     * @param array $array Variables will be stored in this array.
     */
    public function parseStr($string, &$array)
    {
        parse_str($string, $array);
        if (get_magic_quotes_gpc())
            $array = stripslashes_deep($array);
    }
}