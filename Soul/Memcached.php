<?php
namespace Soul;

class Memcached
{
    /**
     * @var array
     */
    private static $_connections;

    /*
     * @param string $host
     * @param array $params
     * @return object \Memcache
     */
    public static function getConnection($host = 'master', array $params)
    {

        if (!isset(self::$_connections[$host])) {
            try {
                $mc = new \Memcached($host);
                $mc->addServer($params['host'], $params['port']);
                $mc->setOption(\Memcached::OPT_PREFIX_KEY, $params['prefix']);
            } catch (Exception $e) {
                throw new Exception($e->getMessage());
            }
            self::$_connections[$host] = $mc;
        }
        return self::$_connections[$host];
    }

}
