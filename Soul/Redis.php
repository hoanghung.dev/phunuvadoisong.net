<?php
namespace Soul;
class Redis
{
    /**
     * @var array $connections
     */
    private static $connections = array();

    /*
     * @param string $host
     * @param array $params
     * @return object \Redis
     */
    public static function factory($host = 'master', array $params)
    {

        if (!isset(self::$connections[$host])) {
            try {
                $redis = new \Redis();
                $redis->connect($params['host'], $params['port'], $params['timeout']); // 2.5 sec timeout.
            } catch (\RedisException $e) {
                throw new Exception($e->getMessage());
            }
            self::$connections[$host] = $redis;
        }
        return self::$connections[$host];
    }

}
