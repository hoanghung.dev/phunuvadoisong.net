<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:05 SA
 */
namespace Crawl;
class Vietnamnet extends Base
{
    public function __construct($page = 2){
        print "Crawler Vietnamnet ...... \n";
        for($i = 1; $i < $page; $i--){
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/thoi-su/trang%d/index.html',$i),15);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/the-gioi/trang%d/index.html',$i),16);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/the-gioi/the-gioi-do-day/trang%d/index.html',$i),17);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/giai-tri/the-gioi-sao/trang%d/index.html',$i),2);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/giai-tri/nhac/trang%d/index.html',$i),18);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/giai-tri/thoi-trang/trang%d/index.html',$i),30);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/the-thao/bong-da-trong-nuoc/trang%d/index.html',$i),40);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/the-thao/bong-da-quoc-te/trang%d/index.html',$i),41);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/the-thao/hau-truong/trang%d/index.html',$i),42);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/doi-song/me-va-be/trang%d/index.html',$i),26);
            print "\n \n";
            $this->getCategory(sprintf('http://vietnamnet.vn/vn/chuyen-trang/oto-xemay/trang%d/index.html',$i),37);

        }
        //$this->getCategory('http://vietnamnet.vn/vn/thoi-su/trang2/index.html',15);
        //$this->getDetail('http://vietnamnet.vn/vn/the-thao/bong-da-trong-nuoc/324297/cong-phuong-khien-cau-thu-mito-nga-mu-voi-pha-solo-ghi-ban-tuyet-dep.html', 26);
    }
    function getCategory($url,$cateId){
        print "Crawl ".$url." \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if(!empty($html)){
            $newsModel = new \Application\Admin\Models\News();
            $data = array();
            foreach($html->find('ul.ListArticle li') as $key=>$article) {
                if($article->find('a', 0)){
                    $link = "http://vietnamnet.vn".$article->find('h3 a', 0)->href;
                    $data[] = $this->getDetail($link,$cateId);
                    print $link."\n";
                }
            }
            $data = array_values(array_filter($data) );
            if(!empty($data) && $newsModel->insert($data) == true) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
            unset($data);$html->clear();
        } else print "Don't get html category \n";
    }

    public function getDetail($url,$cateId){
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
        if(!empty($html)){
            $meta = $this->getMetaTags($html);
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = str_replace(' - VietNamNet','',$meta['title']);
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == false){
                $data['intro']  = $meta['description'];
                /*$data['intro'] = str_replace('(Thethaovanhoa.vn) - ','',$data['intro']);*/
                $data['keywords']  = $meta['keywords'] != null?$meta['keywords']:$data['title'];

                if(!empty($meta['image'])) { $image = $meta['image'];
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;

                    //preg_match("/http\:\/\/media\.vnanet(.*?)\.mp4/x", $html, $video);
                    preg_match_all("/href=\"(.*?\.mp4)/x", $html, $video);
                    //print_r($output_array);exit;
                    $data['link'] = isset($video[1][1])?$video[1][1]:'';
                    //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                    //$data['content']  = $html->find('div.cont',0)->innertext;
                    preg_match("/<div id=\"ArticleContent\" class=\"ArticleContent\">(.*?)<\/div>/i", $html, $output_array);
                    $data['content'] = $output_array[1];
                    /*$data['content'] = str_replace('(Thethaovanhoa.vn) – ','',$data['content']);*/
                    $data['content'] = preg_replace("/<a[^>]*?>.*?<\/a>/i", "$2", $data['content']);
                    $data['content'] = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $data['content']);
                    $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
                    $data['content'] = preg_replace('#<img(.*?)logo.gif(.*?)\/>#is', '', $data['content']);
                    //$data['content'] = preg_replace('/<div class="new_relation_top(.*?)>.*?<\/div>/s','',$data['content']);

                    /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                        $image = $img->src;
                        $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                        $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                        $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                    }*/
                    //print_r($data);
                    return $data;
                }
            }
        }
    }

    function getDetail_($url,$cateId){

        print "Crawl ".$url." \n";
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
        if(!empty($html)){
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = trim($html->find("meta[name=fb_title]",0)->getAttribute('content'));
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == false){
                print $data['slug']." exist !\n";
            }else{
                $data['intro']  = $html->find('meta[name="description"]',0)->getAttribute('content');
                //$data['intro'] = str_replace('(Vietnamnet)','',$data['intro']);
                $data['keywords']  = $html->find("meta[name=keywords]",0)->getAttribute('content');

                if(!empty($html->find('meta[property="og:image"]'))) { $image = $html->find('meta[property="og:image"]',0)->getAttribute('content');
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;
                }
                //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                $data['content']  = $html->find('div#ArticleContent',0)->innertext;
                $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);
                //$data['content'] = preg_replace('/<iframe(.*?)>(.*?)\<\/iframe\>/s', '', $data['content']);
                $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
                $data['content'] = preg_replace('/<img(.*?)logo-small(.*?).*?\>/s','',$data['content']);

                /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                    $image = $img->src;
                    $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                    $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                    $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                }*/
                /*if($newsModel->insert($data)) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
                unset($data);$html->clear();*/
                print_r($data);
            }

        }else print "Don't get html detail\n";
    }
}