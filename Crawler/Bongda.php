<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:05 SA
 */
namespace Crawl;
class Bongda extends Base
{
    public function __construct(){
        print "Crawler Bongda ...... \n";
        for($i = 1; $i < 2; $i++){
            $this->getCategory(sprintf('http://www.bongda.com.vn/bong-da-anh/p%d',$i),14);
            print "\n \n";
            $this->getCategory(sprintf('http://www.bongda.com.vn/bong-da-tbn/p%d',$i),14);
            print "\n \n";
            $this->getCategory(sprintf('http://www.bongda.com.vn/nguoi-dep/p%d',$i),14);
        }

        //$this->getCategory('http://www.bongda.com.vn/bong-da-anh/p2',1);
        //$this->getDetail('http://www.bongda.com.vn/goc-tuyen-trach-cung-soi-tien-dao-sap-cap-ben-arsenal-d357489.html', 14);
    }
    function getCategory($url,$cateId){
        print "Crawl ".$url." \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if(!empty($html)) foreach($html->find('ul.list_news_cate li') as $article) {
            if(!empty($article)){
                $link = $article->find('a', 0)->href;
                $this->getDetail($link,$cateId);
                //print $link."\n";
            }
        }else print "Don't get html category \n";
        $html->clear();
    }
    function getDetail($url,$cateId){
        $newsModel = new \Application\Admin\Models\News();
        print "Crawl ".$url." \n";
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
//print $html->find('meta[property="og:image"]',0)->getAttribute('content');exit;
        if(!empty($html)){
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = $html->find("meta[property=og:title]",0)->getAttribute('content');
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == true){
                print $data['slug']." exist !\n";
            }else{
                $data['intro']  = $html->find('meta[name="description"]',0)->getAttribute('content');
                $data['intro'] = str_replace('(Bongda)','',$data['intro']);
                $data['keywords']  = $html->find("meta[name=keywords]",0)->getAttribute('content');

                if(!empty($html->find('meta[property="og:image"]'))) { $image = $html->find('meta[property="og:image"]',0)->getAttribute('content');
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;


                    //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                    $data['content']  = $html->find('div.exp_content',0)->innertext;
                    $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);
                    //$data['content'] = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $data['content']);
                    $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
                    $data['content'] = preg_replace('/<div class="new_relation_top(.*?)>.*?<\/div>/s','',$data['content']);

                    /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                        $image = $img->src;
                        $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                        $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                        $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                    }*/
                    if($newsModel->insert($data)) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
                    unset($data);$html->clear();
                    //print_r($data);
                }

            }

        }else print "Don't get html detail\n";
    }
}