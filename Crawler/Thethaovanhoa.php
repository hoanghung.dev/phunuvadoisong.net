<?php

/**
 * Created by PhpStorm.
 * User: STEVEN
 * Date: 18/08/2016
 * Time: 12:05 SA
 */
namespace Crawl;
class Thethaovanhoa extends Base
{
    public function __construct($page=2){
        print "Crawler Thethaovanhoa ...... \n";
        for($i = 1; $i < $page; $i--){
            $this->getCategory(sprintf('http://thethaovanhoa.vn/bong-da-trong-nuoc-128ct0/trang-%d.htm',$i),40);
            print "\n \n";
            $this->getCategory(sprintf('http://thethaovanhoa.vn/bong-da-anh-149ct0/trang-%d.htm',$i),41);
            print "\n \n";
            $this->getCategory(sprintf('http://thethaovanhoa.vn/tay-ban-nha-151ct0/trang-%d.htm',$i),41);
        }
        //$this->getCategory('http://thethaovanhoa.vn/tay-ban-nha-151ct0/trang-1.htm',14);
        //print_r($this->getDetail('http://thethaovanhoa.vn/italy/pescara-12-inter-icardi-lap-cu-dup-giup-inter-thang-tran-dau-tien-n20160912063446707.htm', 14));
    }
    public function getCategory($url,$cateId){
        print "Crawl ".$url." \n";
        $html = $this->cUrl($url);
        $html = str_get_html($html);
        if(!empty($html)){
            $newsModel = new \Application\Admin\Models\News();
            $data = array();
            foreach($html->find('.lst ul li') as $key=>$article) {
                if($article->find('h3 a', 0)){
                    $link = "http://thethaovanhoa.vn".$article->find('h3 a', 0)->href;
                    $data[] = $this->getDetail($link,$cateId);
                    print $link."\n";
                }
            }
            $data = array_values(array_filter($data));
            //print_r($data);
            if(!empty($data) && $newsModel->insert($data) == true) print "Insert '.$url.' done !\n";else print "Insert '.$url.' unsuccess !\n";
            unset($data);$html->clear();
        } else print "Don't get html category \n";
    }
    public function getDetail($url,$cateId){
        $dom = $this->cUrl($url);
        $html = str_get_html($dom);
        if(!empty($html)){
            $meta = $this->getMetaTags($html);
            $data['category_id'] = $cateId;
            $data['user_id'] = 1;
            $data['status'] = 1;
            $data['source'] = $url;
            $data['title']  = $meta['title'];
            $data['title_page']  = $data['title'];
            $data['slug'] = $this->toSlug($data['title']);

            if($this->checkExist($data['slug']) == false){
                $data['intro']  = $meta['description'];
                $data['intro'] = str_replace('(Thethaovanhoa.vn) - ','',$data['intro']);
                $data['keywords']  = $meta['keywords'] != null?$meta['keywords']:$data['title'];

                if(!empty($meta['image'])) { $image = $meta['image'];
                    $path = '/'.date('Ym').'/'; // Directory to upload files to.
                    $thumb = $this->uploadImageURL($image,$path,$data['slug']);
                    $data['image'] = $thumb;

                    preg_match("/http\:\/\/media\.vnanet(.*?)\.mp4/x", $html, $video);
                    $data['link'] = isset($video[0])?$video[0]:'';
                    //$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
                    //$data['content']  = $html->find('div.cont',0)->innertext;
                    preg_match("'<div class=\"clearfix cont\">(.*?)</div>'si", $html, $output_array);
                    $data['content'] = $output_array[1];
                    $data['content'] = str_replace('(Thethaovanhoa.vn) – ','',$data['content']);
                    $data['content'] = preg_replace("/\<a([^>]*)\>([^<]*)\<\/a\>/i", "$2", $data['content']);
                    $data['content'] = preg_replace('#<iframe(.*?)>(.*?)</iframe>#is', '', $data['content']);
                    $data['content'] = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $data['content']);
                    //$data['content'] = preg_replace('/<div class="new_relation_top(.*?)>.*?<\/div>/s','',$data['content']);

                    /*if(!empty($data['content'])) foreach(str_get_html($data['content'])->find("img") as $i=>$img) {
                        $image = $img->src;
                        $path = '/'.date('Ym').'/content/'; // Directory to upload files to.
                        $directory = $this->uploadImageURL($image,$path,$data['slug'].'-'.$i);
                        $data['content'] = str_replace($image, _ROOT_UPLOAD.$directory, $data['content']);
                    }*/
                    return $data;
                }
            }
        }
    }
}