$(document).ready(function(){

    $('#keyword').keydown(function(e){
        if(e.keyCode == 13) {
            var keyword = $('#keyword').val();
            if(keyword.length > 0) {
                var domain = location.hostname;
                location.href = '/search/' + keyword;

            } else {
                alert('Vui lòng nhập từ khóa để tìm kiếm!');
            }
        }
    });

});