<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 15/03/2014
 * Time: 10:53
 */
namespace Application\Admin\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class News extends Model
{
    protected  $_tbl ='default_news';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
    }

    public function deleteDuplicate(){
        $sql = 'DELETE FROM default_news WHERE news_id IN (SELECT news_id FROM default_news GROUP BY slug HAVING COUNT(*) >1)';
        $st = $this->_mysql->prepare($sql);
        return $st->execute();
    }

    public function getAll()
    {
        return $this->_mysql->select($this->_tbl());
    }

    public function getOne($where, $bind,$select='*')
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s LIMIT 1',$select,$this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        $st->execute($bind);
        return $st->fetch(\PDO::FETCH_OBJ);
    }
    public function getCount($args = null)
    {
        $status = '';
        $category_id = '';
        $is_hot = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','status'=>0,'category_id'=>0,'is_hot'=>0,'not_in' => 0, 'in' => 0,'search'=> null);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
            );
        }

        if ($category_id != 0){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
            );
        }
        if ($is_hot != 0){
            $where .= ' AND is_hot = :is_hot';
            $bind[] = array(
                'element'=>':is_hot',
                'value'=>$is_hot,
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
            );
        }

        if($search != null){
            $where .= ' AND MATCH(title,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }
        $sql = sprintf('SELECT COUNT(1) FROM %s %s', $this->_tbl, $where);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchColumn();
        return $data;
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $status = '';
        $category_id = '';
        $is_hot = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();

        $default = array('select'=>'*','status'=>0,'category_id'=> 0,'is_hot'=>0, 'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1=1';

        if ($status != null){
            $where .= ' AND status = :status';
            $bind[] = array(
                'element'=>':status',
                'value'=>$status,
            );
        }

        if ($category_id != null){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
            );
        }

        if ($is_hot != null){
            $where .= ' AND is_hot = :is_hot';
            $bind[] = array(
                'element'=>':is_hot',
                'value'=>$is_hot,
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
            );
        }


        if($search != null){
            $where .= ' AND MATCH(title,keywords) AGAINST (:search)';
            $bind[] = array(
                'element'=>':search',
                'value'=>$search,
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';
        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        $st = $this->_mysql->prepare($sql);
        if(is_array($bind)) foreach($bind as $item){
            $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
        }
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;

    }
    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete( $where, $bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}