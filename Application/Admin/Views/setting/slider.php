<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý nội dung
            <small>Danh sách game HTML5</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý nội dung</a></li>
            <li class="active">Danh sách game HTML5</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Danh sách game HTML5</h3>
                        <button type="button" class="btn btn-primary showInsert"> Thêm Slider</button>
                        <span class="ajax-loader hidden"><img src="../images/ajax-loader-3.gif"> </span>
                        <div class="box-tools">
                            <div class="input-group" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php echo $this->flash->message(); ?>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th class="text-center">Vị trí</th>
                                <th class="text-center">Ảnh</th>
                                <th class="text-center">Tiêu đề slider</th>
                                <th class="text-center">Link</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                            </thead>
                            <tbody class="text-center">
                            <?php if($this->data) foreach($this->data as $item): ?>
                                <tr data-id="<?php echo $item->setting_id; ?>">
                                    <td><?php echo $item->setting_id; ?></td>
                                    <td><input name="position" type="text" value="<?php echo $item->position; ?>"></td>
                                    <td>
                                        <div id="image-holder"></div>
                                        <input id="fileUpload" name="image" class="file" type="file">
                                    </td>
                                    <td><input name="title" type="text" value="<?php echo $item->title; ?>"></td>
                                    <td><input name="link" type="text" value="<?php echo $item->link; ?>"></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-circle btnUpdate"><i class="fa fa-repeat"></i></button>
                                        <button type="button" class="btn btn-danger btn-circle btnDelete"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                <tr class="add-slider hidden">
                                    <td>..</td>
                                    <td><input name="position" type="text"></td>
                                    <td>
                                        <img class="previewing" onerror="imgError(this);" style="width: 80px;"/>
                                        <div class="fileUpload btn btn-primary">
                                            <i class="fa fa-file-image-o fa-w"></i> Chọn ảnh
                                            <input type="file" class="upload btnUploadImage" />
                                        </div>
                                    </td>
                                    <td><input name="title" type="text"></td>
                                    <td><input name="link" type="text"></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-success btn-circle btnInsert"><i class="fa fa-plus-circle"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin center-block">
                            <?php echo isset($this->page)?$this->page:''; ?>
                        </ul>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>