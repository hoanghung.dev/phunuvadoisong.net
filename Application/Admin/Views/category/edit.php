<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
$item = $this->data;
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý chuyên mục
            <small>Cập nhật</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý chuyên mục</a></li>
            <li class="active">Cập nhật</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cập nhật chuyên mục</h3>
                        <?php echo $this->flash->message();?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tên chuyên mục</label>
                                <input type="text" name="title" class="form-control" placeholder="Tên chuyên mục" value="<?php echo $item->title; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Tên chuyên mục SEO</label>
                                <input type="text" name="title_page" class="form-control" placeholder="Tên chuyên mục SEO" value="<?php echo $item->title_page; ?>" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="intro" class="form-control" rows="3" required><?php echo $item->intro; ?></textarea>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Class Styles</label>
                                <input type="text" name="class" class="form-control" placeholder="Tên class (Admin tự thêm)" value="<?php echo $item->class; ?>">
                            </div>
                            <div class="form-group">
                                <label>Danh mục cha</label>
                                <select name="parent_id" class="form-control">
                                    <?php $listParent = $this->selectCategory($item->category_id); ?>
                                </select>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Cập nhật</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>