<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:01 CH
 */
$item = $this->data;
?>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Quản lý MT hệ thống</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
<?php if (!empty($item)): ?>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Sửa MT hệ thống <?php echo $item->DESC; ?>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="myForm" data-toggle="validator" role="form"
                                  action="/message/edit/<?php echo $item->message_id; ?>" method="POST"
                                  enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Mã MT</label>
                                    <input type="text" name="CODE" class="form-control" placeholder="Tên MT hệ thống" required value="<?php echo $item->CODE; ?>">
                                    <span class="help-block with-errors"></span>
                                </div>

                                <div class="form-group">
                                    <label>Mô tả</label>
                                    <textarea name="DESC" class="form-control" rows="3" required><?php echo $item->DESC; ?></textarea>
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="form-group">
                                    <label>Nội dung</label>
                                    <textarea name="CONTENT" class="form-control" rows="3" required><?php echo $item->CONTENT; ?></textarea>
                                    <span class="help-block with-errors"></span>
                                </div>


                                <div class="form-group">
                                    <label>Ghi chú</label>
                                    <input type="text" name="COMMENT" class="form-control" placeholder="Ghi chú" value="<?php echo $item->COMMENT; ?>">
                                </div>

                                <div class="form-group">
                                    <div class="form-group">
                                        <label>KÍCH HỌAT</label>
                                        <input class="switch-check" type="checkbox" name="STATUS" <?php if($item->STATUS == 1) echo "checked"; ?>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-default">Cập nhật</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

<?php endif; ?>