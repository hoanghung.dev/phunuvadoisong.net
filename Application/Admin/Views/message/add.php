<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 08:03 CH
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Quản lý MT hệ thống</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Thêm mới MT hệ thống
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="myForm" data-toggle="validator"  role="form" action="/message/add" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Mã MT</label>
                                <input type="text" name="CODE" class="form-control" placeholder="Tên MT hệ thống" required>
                                <span class="help-block with-errors"></span>
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="DESC" class="form-control" rows="3" required></textarea>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="CONTENT" class="form-control" rows="3" required></textarea>
                                <span class="help-block with-errors"></span>
                            </div>


                            <div class="form-group">
                                <label>Ghi chú</label>
                                <input type="text" name="COMMENT" class="form-control" placeholder="Ghi chú">
                            </div>

                            <div class="form-group">
                                <div class="form-group">
                                    <label>KÍCH HỌAT</label>
                                    <input class="switch-check" type="checkbox" name="STATUS">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-default">Thêm mới MT</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">
    /*$("#title").keyup(function(){
        var q = $(this).val();
        $('ul.list-result').removeClass('hide');

        $.ajax({
            url: '<?php echo _ROOT_CMS; ?>/search/'+q,
            success: function(response){
                $('.list-result').html(response);
            }
        })
    });
    $(document).on('click','ul.list-result li',function (event) {
        var id = $(this).attr('data-id');

        $.ajax({
            url: '<?php echo _ROOT_CMS; ?>/get-data-new',
            data: {id:id},
            success: function(response){
                var obj = $.parseJSON(response);
                $('input[name="title"]').val(obj.title);
                $('input[name="parent_id"]').val(obj.news_id);
                $("input#title").val(obj.title);
                $('input[name="title_page"]').val(obj.title_page);
                $('select[name="category_id"] option[value="'+obj.category_id+'"]').attr("selected","selected");
                $('select[name="os_id"] option[value="'+obj.os_id+'"]').attr("selected","selected");
                $('img[name="image"]').attr('src','<?php echo _ROOT_HOME; ?>/upload/'+obj.image);
                $('input[name="keywords"]').val(obj.keywords);
                $('textarea[name="intro"]').val(obj.intro);
                $('textarea[name="content"]').val(obj.content);
                if($('input[name="trial"]')){
                    $('input[value="'+obj.trial+'"]').attr('checked','checked');
                }
                $('ul.list-result').addClass('hide');


            }
        })
    });

    $('.btnShowVersion').click(function(){
        $('.show-version').toggleClass('hide');
        $('.show-news').toggleClass('hide');
    })*/

    /*tinymce.init({
        selector: "textarea.tinymce",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });*/
</script>