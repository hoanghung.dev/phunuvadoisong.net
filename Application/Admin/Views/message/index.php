<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Quản lý MT hệ thống</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">


    <?php if($this->data): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Danh sách MT hệ thống (<?php if($this->countAll) echo $this->countAll; ?>)
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>CODE</th>
                        <th>Mô tả</th>
                        <th>Nội dung</th>
                        <th>Comment</th>
                        <th class="text-center">ACTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->data as $item): ?>
                        <tr data-id="<?php echo $item->message_id; ?>">
                            <td><?php echo $item->CODE; ?></td>
                            <td><?php echo $item->DESC; ?></td>
                            <td><?php echo $item->CONTENT; ?></td>
                            <td><?php echo $item->COMMENT; ?></td>
                            <td class="col-lg-2 text-center">
                                <button type="button" class="btn btn-primary btn-circle" onclick="javascript: window.open('<?php echo $this->getUrlNews($item->message_id); ?>','_blank');"><i class="fa fa-link"></i></button>
                                <button data-access="message_edit" type="button" class="btn btn-success btn-circle" onclick="javascript: window.open('/message/edit/<?php echo $item->message_id; ?>','_blank');"><i class="fa fa-pencil"></i></button>
                                <button data-access="message_delete" type="button" class="btn btn-danger btn-circle <?php if($_GET['STATUS'] == 0) echo 'btnDelete';else echo 'btnTrash'; ?>"><i class="fa fa-times"></i></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="col-lg-5 text-center">
                    <?php echo $this->paging; ?>
                </div>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
<?php endif; ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('.btnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa tạm message này không ?") == true){
                $.ajax({
                    url: '/message/actTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnUnTrash').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn khôi phục message này không ?") == true){
                $.ajax({
                    url: '/message/actUnTrash',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
        $('.btnDelete').click(function(){
            var id = $(this).parents('tr').attr('data-id');
            if(confirm("Bạn có chắc chắn xóa message này khỏi hệ thống không ?") == true){
                $.ajax({
                    url: '/message/actDelete',
                    data: {id:id},
                    success: function(response){
                        alert(response);
                        location.reload();
                    }
                })
            }

        })
    })
</script>