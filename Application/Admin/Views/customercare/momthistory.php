<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 22/07/2015
 * Time: 10:49 SA
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Chăm sóc khách hàng</h1>
    </div>
    <?php echo $this->render('/_layouts/_filter.php'); ?>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            Lịch sử MOMT
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Số thuê bao</th>
                                    <th>MO/MT</th>
                                    <th>Nội dung MO/MT</th>
                                    <th>Ngày gửi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($this->data)): $i = 0; foreach($this->data as $oneItem): $i++; ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo isset($oneItem->MSISDN)?$oneItem->MSISDN:''; ?></td>
                                        <td><?php if(isset($oneItem->MOMT) && $oneItem->MOMT == 0) echo "MO"; else echo "MT"; ?></td>
                                        <td><?php echo isset($oneItem->CONTENT)?$oneItem->CONTENT:''; ?></td>
                                        <td><?php echo isset($oneItem->CREATED_TIME)?date('d/m/Y H:i:s',strtotime($oneItem->CREATED_TIME)):''; ?></td>
                                    </tr>
                                <?php endforeach; else: ?>
                                    <tr><td colspan="7" class="text-center">Không có thông tin !</td></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.panel -->
            </div>
        </div>

    </div>
</div>