<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 22/07/2015
 * Time: 10:49 SA
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Chăm sóc khách hàng</h1>
    </div>
    <?php echo $this->render('/_layouts/_filter_msisdn.php'); ?>
</div>
<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">
            Thông tin chi tiết thuê bao <?php echo isset($_GET['msisdn'])?$_GET['msisdn']:''; ?>
        </div>
            <div class="panel-body">
                <div class="col-lg-10">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Thông tin đăng ký</th>
                                        <th>Tình trạng đăng ký</th>
                                        <th>Ngày đăng ký/hủy</th>
                                        <th>Ngày sử dụng dịch vụ gần nhất</th>
                                        <th>Kênh đăng ký</th>
                                        <th>Kênh hủy</th>
                                        <th>Tổng số download</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($this->data) && $this->data->errorCode == 0): $oneItem = $this->data->subscriber;
                                        $status = '';
                                        switch($oneItem->status){
                                            case 1:
                                                $status = 'Đang hoạt động';
                                                break;
                                            case 3:
                                                $status = 'Đang hủy';
                                                break;
                                        }
                                        ?>
                                            <tr>
                                                <td><?php echo isset($_GET['msisdn'])?$_GET['msisdn']:''; ?></td>
                                                <td><?php echo isset($status)?$status:''; ?></td>
                                                <td><?php echo isset($oneItem->activeTime)?date('d/m/Y',strtotime($oneItem->activeTime)):''; ?><i class="fa fa-ellipsis-h"></i> <?php echo isset($oneItem->deactiveTime)?date('d/m/Y',strtotime($oneItem->deactiveTime)):''; ?></td>
                                                <td><?php echo isset($oneItem->lastChargeSuccess)?date('d/m/Y',strtotime($oneItem->lastChargeSuccess)):''; ?></td>
                                                <td><?php echo isset($oneItem->activeChannel)?$oneItem->activeChannel:''; ?></td>
                                                <td><?php echo isset($oneItem->deactiveChannel)?$oneItem->deactiveChannel:''; ?></td>
                                                <td><?php //echo isset($oneItem->activeTime)?$oneItem->activeTime:''; ?></td>
                                            </tr>
                                        <?php else: ?>
                                            <tr><td colspan="7" class="text-center">Không có thông tin !</td></tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>

                    </div>
                    <!-- /.panel -->
                </div>
            </div>

    </div>
</div>