<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 22/07/2015
 * Time: 10:49 SA
 */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Chăm sóc khách hàng</h1>
    </div>
    <?php echo $this->render('/_layouts/_filter_date.php'); ?>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            Lịch sử thuê bao
        </div>
        <div class="panel-body">
            <div class="col-lg-10">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Ngày</th>
                                    <th>Số thuê bao</th>
                                    <th>Hoạt động</th>
                                    <th>Kênh</th>
                                    <th>Amount</th>
                                    <th>Command</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($this->data)): foreach($this->data as $oneItem): ?>
                                    <tr>
                                        <td><?php echo isset($oneItem->CREATED_TIME)?date('d/m/Y',strtotime($oneItem->CREATED_TIME)):''; ?></td>
                                        <td><?php echo isset($oneItem->MSISDN)?$oneItem->MSISDN:''; ?></td>
                                        <td><?php echo isset($oneItem->ACTION_NAME)?$oneItem->ACTION_NAME:''; ?></td>
                                        <td><?php echo isset($oneItem->CHANNEL)?$oneItem->CHANNEL:''; ?></td>
                                        <td><?php echo isset($oneItem->AMOUNT)?$oneItem->AMOUNT:''; ?></td>
                                        <td><?php echo isset($oneItem->COMMAND)?$oneItem->COMMAND:''; ?></td>
                                    </tr>
                                <?php endforeach; else: ?>
                                    <tr><td colspan="7" class="text-center">Không có thông tin !</td></tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
                <!-- /.panel -->
            </div>
        </div>

    </div>
</div>