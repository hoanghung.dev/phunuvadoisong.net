<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Admin</b>Steven</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b>Steven</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $this->_session->auth->full_name; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                            <p>
                                <?php echo $this->_session->auth->full_name; ?>
                                <small>Member since <?php echo $this->timeAgo($this->_session->auth->created_time,'d/m/Y'); ?></small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/user/<?php echo $this->_session->auth->user_id; ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->_session->auth->full_name; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="treeview">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs fa-fw"></i>
                    <span>Cấu hình hệ thống</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="/setting"><i class="fa fa-cog"></i> Cấu hình chung</a>
                    </li>
                    <li>
                        <a href="/setting/slider"><i class="fa fa-plus-circle"></i> Slider</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users fa-fw"></i>
                    <span>Quản lý người dùng</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="/user"><i class="fa fa-list"></i> Danh sách thành viên</a>
                    </li>
                    <li>
                        <a href="/user/add"><i class="fa fa-plus-circle"></i> Thêm thành viên</a>
                    </li>
                </ul>
            </li>
            <!--<li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap fa-fw"></i>
                    <span>Phân quyền người dùng</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="/permission"><i class="fa fa-list"></i> Danh sách chức năng</a>
                    </li>
                    <li>
                        <a href="/permission/permission-group"><i class="fa fa-table"></i> Bảng phân quyền</a>
                    </li>
                    <li>
                        <a href="/permission/add"><i class="fa fa-plus-circle"></i> Thêm chức năng</a>
                    </li>
                </ul>
            </li>-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-th-list fa-fw"></i>
                    <span>Quản lý chuyên mục</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="/category"><i class="fa fa-list"></i> Danh sách chuyên mục</a>
                    </li>
                    <li>
                        <a href="/category/add"><i class="fa fa-plus-circle"></i> Thêm chuyên mục</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gamepad fa-fw"></i>
                    <span>Quản lý nội dung </span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="/article"><i class="fa fa-list"></i> Danh sách nội dung</a>
                    </li>
                    <li>
                        <a href="/article/add"><i class="fa fa-plus-circle"></i> Thêm nội dung</a>
                    </li>
                </ul>
            </li>
            <li class="treeview article">
                <a href="#">
                    <i class="fa fa-newspaper-o fa-fw"></i>
                    <span>Quản lý quảng cáo </span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="article">
                        <a href="/advertise"><i class="fa fa-list"></i> Danh sách quảng cáo</a>
                    </li>
                    <li class="article_add">
                        <a href="/advertise/add"><i class="fa fa-plus-circle"></i> Thêm quảng cáo</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>