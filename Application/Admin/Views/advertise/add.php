<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý Quảng cáo
            <small>Thêm mới</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý Quảng cáo</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thêm mới Quảng cáo</h3>
                        <?php echo $this->flash->message(); ?>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form id="myForm" data-toggle="validator" role="form" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Tên</label>
                                <input type="text" name="name" class="form-control" placeholder="Tên" required>
                                <span class="help-block with-errors"></span>
                            </div>
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label>Kick thước</label>
                                    <select name="size" class="form-control">
                                        <option value="300x600">300x600</option>
                                        <option value="336x280">336x280</option>
                                        <option value="728x90">728x90</option>
                                        <option value="320x100">320x100</option>
                                        <option value="300x250">300x250</option>
                                        <option value="320x250">320x250</option>
                                        <option value="320x50">320x50</option>
                                    </select>
                                    <span class="help-block with-errors"></span>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>Vị trí</label>
                                    <select name="position" class="form-control">
                                        <option value="top">top</option>
                                        <option value="bottom">bottom</option>
                                        <option value="left">left</option>
                                        <option value="right">right</option>
                                        <option value="content">content</option>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label class="col-xs-12">Trạng thái</label>
                                    <input class="switch-check" type="checkbox" name="status" checked>
                                </div>
                                <div class="col-xs-12 form-group">
                                    <label>Mã quảng cáo</label>
                                    <textarea name="content" rows="10" class="form-control" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-default">Thêm mới</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>