<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 05/05/2015
 * Time: 09:45 SA
 */
?>
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý quảng cáo
            <small>Danh sách quảng cáo</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quản lý quảng cáo</a></li>
            <li class="active">Danh sách quảng cáo</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-name">Danh sách quảng cáo (<?php echo $this->total; ?>)</h3>
                        <div class="box-tools">
                            <div class="input-group" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php echo $this->flash->message();?>
                        <?php if (!empty($this->data)): ?>
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>content</th>
                                </tr>
                                <?php foreach ($this->data as $item):?>
                                <tr data-id="<?php echo $item->advertise_id; ?>">
                                    <td><?php echo $item->advertise_id; ?></td>
                                    <td><?php echo $item->name; ?></td>
                                    <td><?php echo $item->size; ?></td>
                                    <td><?php echo $item->position; ?></td>
                                    <td class=" <?php if($item->status==1) echo 'on'; else echo 'off';?>"><?php if($item->status==1) echo 'On'; else echo 'Off';?></td>
                                    <td><?php print $item->content;?></td>
                                    <td>
                                        <button type="button" class="btn btn-success" onclick="javascript: window.open('/<?php echo $this->params['router']['controller']; ?>/edit/<?php echo $item->advertise_id; ?>','_blank');"><i class="fa fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger btnDelete"><i class="fa fa-times"></i></button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin center-block">
                            <?php echo isset($this->paging)?$this->paging:''; ?>
                        </ul>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div>


<style>
    td.on {
        color: blue;
    }
    td.off {
        color: red;
    }
</style>