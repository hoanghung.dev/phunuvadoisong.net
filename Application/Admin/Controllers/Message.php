<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Messages;

class Message extends Base
{

    public function index(){
        $this->accessDenied('message');
        $newsModel = new Messages();
        $param['select'] = '*';
        $param['page'] = $this->_request->getParam('page')?$this->_request->getParam('page'):1;
        $param['limit'] = 30;
        if($this->_request->getParam('STATUS') != null)
            $param['STATUS'] = $this->_request->getParam('STATUS');

        $totalRecord = $newsModel->getCount($param);

        $this->view->data = $newsModel->getDataArr($param);
        $this->view->countAll = $totalRecord;
        $Pagination = new \Pagination(array('page'=>$param['page'],'total'=>$totalRecord,'limit'=>30,'link'=>'/message'));
        $this->view->paging = $Pagination->createLinks();
        $this->displayLayout('default', $this->render());
    }

    public function add(){
        $this->accessDenied('message_add');
        $newsModel = new Messages();
        if($this->_request->isPost()){
            $data['CODE'] = $this->_request->getPost('CODE');
            $data['DESC'] = $this->_request->getPost('DESC');
            $data['CONTENT'] = $this->_request->getPost('CONTENT');
            $data['COMMENT'] = $this->_request->getPost('COMMENT');
            if($this->_request->getPost('STATUS') == 'on')
                $data['STATUS'] = 1;//Trạng thái mở
            else  $data['STATUS'] = 0; // Trạng thái khóa
            if($newsModel->getOne('CODE = :CODE',array(':CODE'=>$data['CODE']))){
                $this->_flash->addMessage("MT đã tồn tại !");
            }else{

                if(is_array($data)){
                    if($newsModel->insert($data) == true) $this->_flash->addMessage("Thêm mới MT thành công !");
                    else $this->_flash->addMessage("Thêm mới MT không thành công !");
                    unset($data);

                }
            }

        }
        $this->displayLayout('default',$this->render());
    }

    public function edit(){
        $this->accessDenied('message_edit');
        $newsModel = new Messages();
        $id = $this->_request->getParam('id');
        $this->view->data = $oneItem = $newsModel->getOne('message_id = :id',array(':id'=>$id));
        if($this->_request->isPost()){
            $data['CODE'] = $this->_request->getPost('CODE');
            $data['DESC'] = $this->_request->getPost('DESC');
            $data['CONTENT'] = $this->_request->getPost('CONTENT');
            $data['COMMENT'] = $this->_request->getPost('COMMENT');
            if($this->_request->getPost('STATUS') == 'on')
                $user['STATUS'] = 1;//Trạng thái mở
            else  $user['STATUS'] = 0; // Trạng thái khóa
            if(is_array($data)){
                if($newsModel->update($data,'message_id = :id',array(':id'=>$id)) == true) $this->_flash->addMessage("Cập nhật MT thành công !");
                else $this->_flash->addMessage("Cập nhật MT không thành công !");
                unset($data);

            }

        }

        $this->displayLayout('default',$this->render());

    }

    public function actTrash(){
        $newsModel = new Messages();
        $id = $this->_request->getParam('id');
        if($newsModel->update(array('STATUS' => 0),'message_id = :id',array(':id'=>$id))) print 'Xóa tạm nội dung thành công !';
        else print 'Xóa tạm nội dung không thành công !';
        exit;
    }
    public function actUnTrash(){
        $newsModel = new Messages();
        $id = $this->_request->getParam('id');
        if($newsModel->update(array('STATUS' => 1),'message_id = :id',array(':id'=>$id))) print 'Khôi phục MT thành công !';
        else print 'Khôi phục MT không thành công !';
        exit;
    }
    public function actDelete(){
        $newsModel = new Messages();
        $id = $this->_request->getParam('id');
        if($newsModel->delete('message_id = :id',array(':id'=>$id))) print 'Xóa nội dung thành công !';
        else print 'Xóa nội dung không thành công !';
        exit;
    }
}