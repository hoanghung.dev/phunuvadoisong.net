<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Api;
use Application\Admin\Models\Movies;
use Application\Admin\Models\Report;

class CustomerCare extends Base
{
    public function index()
    {
        $this->accessDenied('customer_care');
        $this->displayLayout('default', $this->render());
    }
    public function subscriberDetail(){
        if($this->_request->getParam('msisdn')){
            $API = new Api();
            $msisdn = $this->_request->getParam('msisdn');

            if($msisdn != '' && $msisdn[0] === '0'){
                $msisdn = preg_replace("/^0/", "84", $msisdn);
            }
            $data = $API->getSubscriber($msisdn);
            $this->view->data = $data;
        }
        $this->displayLayout('default', $this->render());
    }
    public function subscriberHistory(){
        $startDate = $this->_request->getParam('startDate');
        $endDate = $this->_request->getParam('endDate');
        $page = $this->_request->getParam('page',1);
        if($startDate != '' && $endDate != ''){
            $tables = $this->getTableNameFromMonth('HIS_',$startDate,$endDate);
            $startDate = date('Y-m-d 00:00:00',strtotime($this->_request->getParam('startDate')));
            $endDate = date('Y-m-d 23:59:59',strtotime($this->_request->getParam('endDate')));
            $dataModel = new Report();


            $select = 'CREATED_TIME,MSISDN,ACTION_NAME,CHANNEL,AMOUNT,COMMAND';
            $orderBy = ' ORDER BY CREATED_TIME DESC';
            $limit = 20;
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf(' LIMIT %d,%d',$offset,$limit);

            if(count($tables) > 1){
                $i = 0;
                $sql = '';
                foreach($tables as $table){
                    $i++;
                    $sql .= sprintf('SELECT %s FROM %s',$select,$table);
                    $sql .= ' WHERE DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
                    $bind[] = array(
                        'element'=>':start_date',
                        'value'=>$startDate,
                    );
                    $bind[] = array(
                        'element'=>':end_date',
                        'value'=>$endDate,
                    );
                    if($i < count($tables)) $sql .= ' UNION ALL ';
                }
            }else{
                $sql = sprintf('SELECT %s FROM %s',$select,$tables[0]);
                $sql .= ' WHERE DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
                $bind[] = array(
                    'element'=>':start_date',
                    'value'=>$startDate,
                );
                $bind[] = array(
                    'element'=>':end_date',
                    'value'=>$endDate,
                );
            }
            // $count = $dataModel->countQuery($sql,$bind);
            $sql .= $orderBy.$limit;
//echo $sql;exit;
            $data = $dataModel->queryAll($sql,$bind);
            $this->view->data = $data;
        }
        $this->displayLayout('default', $this->render());
    }
    public function MOMTHistory(){
        $msisdn = $this->_request->getParam('msisdn');

        if($msisdn != '' && $msisdn[0] === '0'){
            $msisdn = preg_replace("/^0/", "84", $msisdn);
        }
        $startDate = $this->_request->getParam('startDate');
        $endDate = $this->_request->getParam('endDate');
        $page = $this->_request->getParam('page',1);
        if($startDate != '' && $endDate != ''){
            $limit = 20;
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $data = $this->getMoMT($msisdn,$startDate,$endDate,$offset,$limit);
            $this->view->data = $data;
        }
        $this->displayLayout('default', $this->render());
    }
    public function sendMovies(){
        $this->displayLayout('default', $this->render());
    }
    public function sendLinkHot(){
        $this->displayLayout('default', $this->render());
    }
    public function registerMoviesHot(){
        $this->displayLayout('default', $this->render());
    }
    private function getMoMT($msisdn,$startDate,$endDate, $offset, $limit){
        $dataModel = new Report();
        $startDateBind = date('Y-m-d 00:00:00',strtotime($startDate));
        $endDateBind = date('Y-m-d 23:59:59',strtotime($endDate));
//GET DATA MO
        $tableMO = $this->getTableNameFromMonth('MO_',$startDate,$endDate);
        $select = 'MSISDN,CONTENT,COMMAND,CREATED_TIME';
        $sqlMO = '';
        $sqlMT = '';
        $bind = array();

        if(count($tableMO) > 1){
            $i = 0;
            foreach($tableMO as $table){
                $i++;
                $sqlMO .= sprintf('SELECT %s,0 AS MOMT FROM %s',$select,$table);
                $sqlMO .= ' WHERE MSISDN = :msisdn AND DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
                $bind[] = array(
                    'element'=>':msisdn',
                    'value'=>$msisdn,
                );
                $bind[] = array(
                    'element'=>':start_date',
                    'value'=>$startDateBind,
                );
                $bind[] = array(
                    'element'=>':end_date',
                    'value'=>$endDateBind,
                );
                if($i < count($tableMO)) $sqlMO .= ' UNION ALL ';
            }
        }else{
            $sqlMO = sprintf('SELECT %s,0 AS MOMT FROM %s',$select,$tableMO[0]);
            $sqlMO .= ' WHERE MSISDN = :msisdn AND DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
            $bind[] = array(
                'element'=>':msisdn',
                'value'=>$msisdn,
            );
            $bind[] = array(
                'element'=>':start_date',
                'value'=>$startDateBind,
            );
            $bind[] = array(
                'element'=>':end_date',
                'value'=>$endDateBind,
            );
        }
//GET DATA MT
        $tableMT = $this->getTableNameFromMonth('MT_',$startDate,$endDate);
        if(count($tableMT) > 1){
            $i = 0;
            foreach($tableMT as $table){
                $i++;
                $sqlMT .= sprintf('SELECT %s,1 AS MOMT FROM %s',$select,$table);
                $sqlMT .= ' WHERE MSISDN = :msisdn AND DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
                $bind[] = array(
                    'element'=>':msisdn',
                    'value'=>$msisdn,
                );
                $bind[] = array(
                    'element'=>':start_date',
                    'value'=>$startDateBind,
                );
                $bind[] = array(
                    'element'=>':end_date',
                    'value'=>$endDateBind,
                );
                if($i < count($tableMT)) $sqlMT .= ' UNION ALL ';
            }
        }else{
            $sqlMT = sprintf('SELECT %s,1 AS MOMT FROM %s',$select,$tableMT[0]);
            $sqlMT .= ' WHERE MSISDN = :msisdn AND DATE(CREATED_TIME) BETWEEN :start_date AND :end_date';
            $bind[] = array(
                'element'=>':msisdn',
                'value'=>$msisdn,
            );
            $bind[] = array(
                'element'=>':start_date',
                'value'=>$startDateBind,
            );
            $bind[] = array(
                'element'=>':end_date',
                'value'=>$endDateBind,
            );
        }
        $queryAll = "$sqlMO UNION ALL $sqlMT ORDER BY CREATED_TIME DESC,MOMT ASC LIMIT $offset,$limit";
        $data = $dataModel->queryAll($queryAll,$bind);
        return $data;
    }
    protected function getTableNameFromMonth($prefix, $dateBegin, $dateEnd){
        $dateBegin = date('Y-m',strtotime($dateBegin));
        $dateEnd = date('Y-m',strtotime($dateEnd));

        $arr_rs = array();
        $arr_rs[] = $prefix . date('Ym',strtotime($dateBegin));
        while (strtotime($dateBegin) < strtotime($dateEnd)) {
            $dateBegin = date ("Y-m", strtotime("+1 month", strtotime($dateBegin)));
            $arr_rs[] = $prefix . date('Ym',strtotime($dateBegin));
        }
        return $arr_rs;
    }
}