<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 01/05/2015
 * Time: 06:54 CH
 */
namespace Application\Admin\Controllers;


use Application\Admin\Models\Advertises;

class Advertise extends Base
{

    public function index()
    {
//        $str = '';
//        $arr = explode('.', $str);
//        if(count($arr) > 1) {
//            $mid = (int) ( count($arr) / 2 );
//            for ($i = count($arr); $i >= $mid; $i --) {
//                $arr[$i] = $arr[$i - 1];
//            }
//            $arr[$mid] = 'giua';
//            $arr = implode('.', $arr);
//        }
//        var_dump($arr); die;

        $AdvertiseModel = new Advertises();
        $page = $this->_request->getParam('page', 1);
        $limit = 30;
        $params['select'] = '*';
        $params['order_by'] = 'advertise_id DESC';
        $params['page'] = $page;
        $params['limit'] = $limit;
        $data = $AdvertiseModel->getDataArr($params);
//        var_dump($data); die;
        $total = $AdvertiseModel->getCount($params);
        if (empty($data)) $this->_flash->warning('Chưa có dữ liệu !');
        $this->view->data = $data;
        $this->view->total = $total;
        $this->view->paging = $this->getPaging($total, $page, $limit, 5);
        $this->displayLayout('default', $this->render());
    }

    public function add()
    {
        $AdvertiseModel = new Advertises();
        if ($this->_request->isPost()) {
            $data = array();
            foreach ($this->_request->getPosts() as $field => $value) {
                $data[$field] = $value;
            }

            if ($this->_request->getPost('status')) $data['status'] = 1; else $data['status'] = 0;

            if (is_array($data)) {
                if ($AdvertiseModel->insert($data) == true) $this->_flash->success("Thêm mới quảng cáo thành công !");
                else $this->_flash->danger("Thêm mới quảng cáo không thành công !");
                unset($data);

            }
            
        }
        $this->displayLayout('default', $this->render());
    }

    public function edit()
    {
        $AdvertiseModel = new Advertises();
        $id = $this->_request->getParam('id');
        if ($this->_request->isPost()) {
            $data = array();
            foreach ($this->_request->getPosts() as $field => $value) {
                $data[$field] = $value;
            }

            if ($this->_request->getPost('status')) $data['status'] = 1; else $data['status'] = 0;

            if (is_array($data)) {
                if ($AdvertiseModel->update($data, 'advertise_id = :id', array(':id' => $id)) == true) $this->_flash->success("Cập nhật quảng cáo thành công !");
                else $this->_flash->danger("Cập nhật quảng cáo không thành công !");
                unset($data);
            }
        }
        $this->view->data = $AdvertiseModel->getOne('advertise_id = :id', array(':id' => $id));

        $this->displayLayout('default', $this->render());
    }

    public function actDelete()
    {
        $AdvertiseModel = new Advertises();
        $id = $this->_request->getParam('id');
        $data = $AdvertiseModel->delete('advertise_id = :id', array(':id' => $id));
        print $data;
        exit;
    }
}