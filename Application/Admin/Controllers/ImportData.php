<?php
namespace Application\Admin\Controllers;

use Application\Admin\Models\Export;
use Application\Admin\Models\Movies;

class ImportData extends Base
{
    public function index(){
        $ExportModel = new Export();
        $cateArray = array(
            0 => array(
                'cateOld' => 95,
                'cateNew' => 6,
                'Title' => 'Phim lẻ'
            ),
            1 => array(
                'cateOld' => 96,
                'cateNew' => 10,
                'Title' => 'Phim hành động'
            ),
            2 => array(
                'cateOld' => 97,
                'cateNew' => 12,
                'Title' => 'Phim viễn tưởng'
            ),
            3 => array(
                'cateOld' => 102,
                'cateNew' => 3,
                'Title' => 'Phim việt nam'
            ),
            4=> array(
                'cateOld' => 98,
                'cateNew' => 17,
                'Title' => 'Phim hình sự '
            ),
            5 => array(
                'cateOld' => 99,
                'cateNew' => 20,
                'Title' => 'Chiến tranh'
            ),
            6 => array(
                'cateOld' => 101,
                'cateNew' => 14,
                'Title' => 'Kinh dị'
            ),
            7 => array(
                'cateOld' => 103,
                'cateNew' => 3,
                'Title' => 'Phim nước ngoài'
            ),
            8 => array(
                'cateOld' => 100,
                'cateNew' => 22,
                'Title' => 'Phim hoạt hình'
            ),
            9 => array(
                'cateOld' => 101,
                'cateNew' => 14,
                'Title' => 'Kinh dị'
            ),

        );
        /*foreach($cateArray as $item){
            $this->getArticle($item['cateOld'],$item['cateNew']);
        }

        //$this->getArticle(96,10);

        $datas = $this->getChild(94);
        foreach($datas as $dataCate){

            $data = $this->getOneUpload($dataCate->id);
            if($this->checkExist($this->toSlug($dataCate->title)) == false){
                $moviesModel = new Movies();
                if(strpos($dataCate->title,"-")){
                    $title = explode("-",$dataCate->title);
                    $dataSave['title'] = trim($title[0]);
                    $dataSave['title_page'] = trim($title[1]);
                }else{
                    $dataSave['title'] = $dataCate->title;
                }

                $dataSave['slug'] = $this->toSlug($dataCate->title);
                $dataSave['is_approval'] = 1;
                if($data->videoPrice != 0)  $dataSave['is_price'] = 1;
                $dataSave['category_id'] = '|7|';
                $dataSave['image_in'] = $data->img_name;
                $dataSave['intro'] = $data->video_desc;
                $dataSave['keywords'] = $data->video_tags;
                $dataSave['country'] = $data->country;
                $timePlay = explode(':',$data->duration);
                $dataSave['time_play'] = $timePlay[0];
                $dataSave['created_time'] = $this->rand_date('2014-01-01 00:00:00',date('Y-m-d H:i:s'));
                $dataSave['updated_time'] = $dataSave['created_time'];
                $lastId = $moviesModel->insertClip24($dataSave);
                if(!empty($lastId)) echo "Thêm bản ghi ".$dataSave['title']." thành công ---</br>";
                $episode = $this->getUpload($dataCate->id);
                //print_r($episode);exit;
                $i = 1;
                if(!empty($episode)) foreach($episode as $item){
                    if($this->insertMoviesEpisode($item,$lastId,$i++,$dataSave['created_time'],$dataSave['updated_time'])) echo "Thêm tập phim ".$dataSave['title']." cho ID ".$lastId." thành công </br>";
                }
            }else{
                echo "Đã tồn tại bản ghi ".$data->title." ! </br>";
            }
        }*/


    return $this->checkFileExist();

    }

    function checkFileExist(){
        $moviesModel = new Movies();
        $listMovies = $moviesModel->getDataArr(array('is_approval'=>1,'limit'=>1000));
       // print_r($listMovies);
        $i = 1;
        $array = array();
        if(!empty($listMovies)) foreach($listMovies as $items){
            $data = $moviesModel->getDataEpisode(array('movies_id' => $items->movies_id,'limit'=>1000));

            if(!empty($data)) foreach($data as $item){
                if (!is_file(DIR_FOLDER_VIDEO.$item->link)) {
                    //echo $i++.": File ".DIR_FOLDER_VIDEO.$item->link." Không tồn tại trên server </br>";
                    $arr = explode('/',$item->link);
                    array_pop($arr);
                    $array[] .= DIR_FOLDER_VIDEO.implode('/',$arr);
                }
            }
        }
        $array = array_unique($array);
        if($array) foreach($array as $item){
            echo $item .'</br>';
        }

    }
    function getOneUpload($cateId){
        $ExportModel = new Export();
        $params['cat_id'] = $cateId;
        $listArticle = $ExportModel->getDataUpload($params);
        return $listArticle[0];
    }
    function getUpload($cateId){
        $ExportModel = new Export();
        $params['cat_id'] = $cateId;
        $listArticle = $ExportModel->getDataUpload($params);
        return $listArticle;
    }
    function getChild($id){
        $ExportModel = new Export();
        $paramCate['select'] = 'id, title, parent_id';
        $paramCate['parent_id'] = $id;
        $listCate = $ExportModel->getDataCategory($paramCate);
        $arr = $listCate;
        /*print_r($listCate);exit;
        if(!empty($listCate)) foreach($listCate as $item){
            if(!empty($item->id) && $item->id !=0) $arr[] = $this->getChild($item->id);
        }*/
        return $arr;
    }
    function getArticle($categoryId,$cateMMovies){
        $ExportModel = new Export();
        $params['cat_id'] = $categoryId;
        $listArticle = $ExportModel->getDataUpload($params);
        if(!empty($listArticle)) foreach($listArticle as $item) {
            $this->insertMovies($item, $cateMMovies);
        }else die("Không có record nào !");

    }

    function insertMovies($data,$cateMMovies){
        if($this->checkExist($this->toSlug($data->title)) == false){
            $moviesModel = new Movies();
            if(strpos($data->title,"-")){
                $title = explode("-",$data->title);
                $dataSave['title'] = trim($title[0]);
                $dataSave['title_page'] = trim($title[1]);
            }else{
                $dataSave['title'] = $data->title;
            }
            $dataSave['slug'] = $this->toSlug($data->title);
            $dataSave['is_approval'] = 1;
            if($data->videoPrice != 0)  $dataSave['is_price'] = 1;
            $dataSave['category_id'] = '|'.$cateMMovies.'|';
            $dataSave['image_in'] = $data->img_name;
            $dataSave['image_out'] = isset($data->img_name2)?$data->img_name2:$data->img_name;
            $dataSave['intro'] = $data->video_desc;
            $dataSave['keywords'] = $data->video_tags;
            $dataSave['country'] = $data->country;
            $timePlay = explode(':',$data->duration);
            $dataSave['time_play'] = $timePlay[0];
            $dataSave['created_time'] = $this->rand_date('2014-01-01 00:00:00',date('Y-m-d H:i:s'));
            $dataSave['updated_time'] = $dataSave['created_time'];
            $lastId = $moviesModel->insertClip24($dataSave);
            if(!empty($lastId)) echo "Thêm bản ghi ".$data->title." thành công ---";
            if($this->insertMoviesEpisode($data,$lastId,1,$dataSave['created_time'],$dataSave['updated_time'])) echo "Thêm tập phim ".$data->title." cho ID ".$lastId." thành công </br>";
        }else{
            echo "Đã tồn tại bản ghi ".$data->title." ! </br>";
        }
    }
    function insertMoviesEpisode($data,$moviesId,$episode,$createdTime,$updatedTime){
        $moviesModel = new Movies();
        $epi['movies_id'] = $moviesId;
        $epi['episode_id'] = $episode;
        if($data->video_1080p != ''){
            $epi['link'] = $data->video_1080p;
        }elseif($data->video_720p != ''){
            $epi['link'] = $data->video_720p;
        }elseif($data->video_480p != ''){
            $epi['link'] = $data->video_480p;
        }else{
            $epi['link'] = $data->video_360p;
        }
        $epi['created_time'] = $createdTime;
        $epi['updated_time'] = $updatedTime;

        return $moviesModel->insertEpisodeClip24($epi);
    }


    function checkExist($slug){
        $moviesModel = new Movies();
        $data = $moviesModel->getOneClip24("slug = :slug",array(':slug'=>$slug));
        if(!empty($data)) return true; else return false;
    }
    function rand_date($min_date, $max_date) {
        /* Gets 2 dates as string, earlier and later date.
           Returns date in between them.
        */

        $min_epoch = strtotime($min_date);
        $max_epoch = strtotime($max_date);

        $rand_epoch = rand($min_epoch, $max_epoch);

        return date('Y-m-d H:i:s', $rand_epoch);
    }
}