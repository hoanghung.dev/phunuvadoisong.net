<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\News;

class Tags extends Base {

    public function index() {
        $newsModel = new News();
        $tag = $this->_request->getParam('slug');
        $this->view->url = $url = '/tags/'.$tag;
        $this->view->tag = $title = urldecode($tag);;
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time';
        $params['search'] = $title;
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 1;

        $total = $newsModel->getCount($params);

        $this->view->listNews = $newsModel->getDataArr($params);
//        var_dump($this->view->listNews); die;
        $activePage = $this->_request->getParam('page',1);
        $this->view->page = $this->getPaging($total,$activePage,$params['limit'],5,$url);
        $SEO['title'] = $title;
        $SEO['link'] = $url;
        $SEO['description'] = "Tag ".$title ;
        $SEO['keywords'] = $title;
        //$SEO['image'] = $this->_helper->getImageCate($data->news_id);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }
}