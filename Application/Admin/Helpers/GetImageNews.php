<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\News;
use Soul\Helpers\Thumbnail;

class GetImageNews extends Base{
    public function getImageNews($id, $width = null, $height = null){
        $newsModel = new News();
        $data = $newsModel->getOne('news_id = ?',array($id),'image');
        if(!is_file(DIR_UPLOAD.'/'.$data->image)) return _ROOT_UPLOAD.'/no-images.jpg';
        else return _ROOT_UPLOAD.'/'.$data->image;
    }
}