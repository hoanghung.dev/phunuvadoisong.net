<?php
namespace Application\Admin\Helpers;
use Application\Admin\Models\Categories;

class CountCategory
{
    public function countCategory($params = null)
    {
        $categoryModel = new Categories();
        $data = $categoryModel->getCount($params);
        return !empty($data)?$data:0;

    }
}