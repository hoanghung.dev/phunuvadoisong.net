<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Models\Categories;

class GetCategory{
    public function getCategory($id,$select='*'){
        $categoryModel = new Categories();
        return $categoryModel->getOne('category_id = :id',array(':id'=>$id),$select);
    }
    private function stringToArray($string){
        $string = str_replace('|',' ',$string);
        $string = trim($string);
        $string = explode(' ',$string);
        return $string;
    }
}