<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 05/03/2014
 * Time: 15:21
 */
namespace Application\Admin\Helpers;

use Soul\Registry;

class GetUserGroup
{
    public function getUserGroup($code = '') {
        $userGroupModel = Registry::get('Group');
        $html = '';
        if(!empty($userGroupModel)) foreach($userGroupModel as $key=>$one) {
            //echo ($one->code);exit;
            $selected = '';
            if($one->code == $code) $selected = 'selected="selected"';
            $html .= '<option value="'.$one->code.'" '.$selected.'>|-- '.$one->name.'</option>';
        }
        print $html;
    }
}


