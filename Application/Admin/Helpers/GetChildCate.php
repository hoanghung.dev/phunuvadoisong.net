<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Admin\Helpers;
use Application\Admin\Controllers\Base;
use Application\Admin\Models\Categories;

class GetChildCate extends Base{
    public function getChildCate($id,$select='*'){
        $categoryModel = new Categories();
        return $categoryModel->findOne(array('parentId'=>new \MongoInt32($id)));
    }
    private function stringToArray($string){
        $string = str_replace('|',' ',$string);
        $string = trim($string);
        $string = explode(' ',$string);
        return $string;
    }
}