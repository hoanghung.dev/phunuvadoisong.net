<?php $oneItem = $this->new; ?>
<div class="cs-container" style="transform: none;">
    <!-- Main content -->
    <div class="cs-main-content cs-sidebar-on-the-right">

        <!-- Post header -->
        <header class="cs-post-single-title">
            <div class="cs-post-category-solid cs-clearfix">
                <?php $oneCate = $this->getCategory($oneItem->category_id, 'title, title_page'); ?>
                <h1><a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>"><?php echo $oneCate->title; ?></a></h1>
            </div>
            <h1><?php echo $oneItem->title; ?></h1>

            <div class="cs-post-meta cs-clearfix">
                <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time, 'd-m-Y'); ?></span>
                <div class="fb-like" data-href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>
        </header>

        <!-- Single post -->
        <article class="cs-single-post" style="transform: none;">
            <!-- Post share -->
            <div class="cs-single-post-share"
                 style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                <div class="theiaStickySidebar"
                     style="padding-top: 0px; padding-bottom: 1px; position: static; top: 25px; left: 131px;">
                    <div>
                        <a rel="nofollow" class="facebook"
                           onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                           href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
                        <a rel="nofollow" class="twitter"
                           onClick="window.open('http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                           href="http://twitter.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&amp;text=<?php echo $this->info->title; ?>"><i class="fa fa-twitter"></i></a>
                        <a rel="nofollow" class="googleplus"
                           onClick="window.open('https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;"
                           href="https://plus.google.com/share?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>"><i class="fa fa-google-plus"></i></a>
                        <a rel="nofollow" class="pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo $this->getUrlNews($oneItem->news_id); ?>&media=<?php echo $this->getImageNews($oneItem->news_id,200,200); ?>&description=<?php echo $oneItem->intro; ?>"><i class="fa fa-pinterest"></i></a>
                        <a rel="nofollow" href="#"><i class="fa fa-envelope"></i></a>
                    </div>
                </div>
            </div>
            <!-- Post content -->
            <div class="cs-single-post-content">

                <?php  print $this->GetAds('top');?>

                <!-- Media -->
                <div class="cs-single-post-media hidden visible cs-animate-element full-visible">
                    <div class="cs-media-credits">Photography <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></div>
                    <a class="cs-lightbox-image" href="<?php echo $this->getImageNews($oneItem->news_id); ?>"
                       title="<?php echo $oneItem->title; ?>"><img
                            src="<?php echo $this->getImageNews($oneItem->news_id); ?>"
                            alt="<?php echo $oneItem->title; ?>"></a>
                </div>
                <!-- Content -->
                <div class="cs-single-post-paragraph">
                    <?php print $oneItem->content; ?>
                </div>

                <?php print $this->GetAds('bottom');?>

                <div id="SC_TBlock_166851" class="SC_TBlock">loading...</div>
                <div>Nguồn: <?php echo str_replace('.',' .',$oneItem->source); ?></div>
                <!-- Tags -->
                <div class="cs-single-post-tags">
                    <span>Tags</span>
                    <?php echo $this->getTag($oneItem->keywords); ?>
                </div>
            </div>
        </article>
        <!-- Controls -->
        <div class="cs-single-post-controls">
            <div class="cs-prev-post">
                <?php if(!empty($this->new_prev)):?>
                <span><i class="fa fa-angle-double-left"></i> Prev</span>
                <a href="<?php echo $this->getUrlNews($this->new_prev[0]->news_id); ?>"><?php echo $this->new_prev[0]->title;?></a>
                <?php endif;?>
            </div>
            <div class="cs-next-post">
                <?php if(!empty($this->new_next)):?>
                <span>Next <i class="fa fa-angle-double-right"></i></span>
                <a href="<?php echo $this->getUrlNews($this->new_next[0]->news_id); ?>"><?php echo $this->new_next[0]->title;?></a>
                <?php endif;?>
            </div>
        </div>

        <!-- Related articles -->
        <div class="cs-single-related-aticles">
            <h4 class="cs-heading-subtitle">Bài viết liên quan</h4>

            <div class="cs-row">
                <?php if(!empty($this->relate_category)) foreach($this->relate_category as $oneItem):?>
                <div class="cs-col cs-col-4-of-12">
                    <!-- Block layout 3 -->
                    <div class="cs-post-block-layout-3">
                        <!-- Post item -->
                        <div class="cs-post-item hidden visible cs-animate-element">
                            <div class="cs-post-thumb">
                                <div class="cs-post-category-border cs-clearfix">
                                    <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                                </div>
                                <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id,215,150); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                            </div>
                            <div class="cs-post-inner">
                                <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                                <div class="cs-post-meta cs-clearfix">
                                    <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                    <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time,'d-m-Y'); ?></span>
                                                <span class="cs-post-meta-rating" title="Rated 3.50 out of 5">
                                                    <span style="width: 70%">3.50 out of 5</span>
                                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

                <?php if (!empty($this->relate_tag)) foreach ($this->relate_tag as $oneItem): ?>
                    <?php $oneCate = $this->getCategory($oneItem->category_id, 'title, title_page'); ?>
                    <div class="cs-col cs-col-4-of-12">
                        <!-- Block layout 3 -->
                        <div class="cs-post-block-layout-3">
                            <!-- Post item -->
                            <div class="cs-post-item hidden visible cs-animate-element">
                                <div class="cs-post-thumb">
                                    <div class="cs-post-category-border cs-clearfix">
                                        <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>"
                                           title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                                    </div>
                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                                       title="<?php echo $oneItem->title; ?>"><img
                                            src="<?php echo $this->getImageNews($oneItem->news_id,215,150); ?>"
                                            alt="<?php echo $oneItem->title; ?>"></a>
                                </div>
                                <div class="cs-post-inner">
                                    <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                                           title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>

                                    <div class="cs-post-meta cs-clearfix">
                                        <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                        <span class="cs-post-meta-date">Jul 1, 2015</span>
                        <span
                            class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time, 'd-m-Y'); ?></span>
                                                <span class="cs-post-meta-rating" title="Rated 3.50 out of 5">
                                                    <span style="width: 70%">3.50 out of 5</span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <!--Comment FB-->
        <div class="fb-comments" data-href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" data-width="100%" data-numposts="5"></div>

    </div>
    <!-- Main sidebar -->
    <?php echo $this->action('boxSidebar', 'Block', 'Frontend');?>
</div>