<div id="cs-header-style-one">
    <!-- Header main -->
    <div id="cs-header-main">
        <div class="cs-container">
            <div class="cs-header-body-table">
                <div class="cs-header-body-row">
                    <!-- Logo brand image -->
                    <div id="cs-logo-brand">
                        <h1>
                            <a href="/" title=" <?php echo isset($this->info->title_page)?$this->info->title_page:''; ?>">
                                <img src="/images/logo.png" alt=" <?php echo isset($this->info->title_page)?$this->info->title_page:''; ?>">
                            </a>
                        </h1>
                    </div>
                    <?php if(!$this->checkMobile()):?>
                    <div class="cs-header-banner">
                        <iframe src="http://ylx-2.com/banner_show_safe.php?section=General&amp;pub=791475&amp;format=728x90&amp;ga=g" frameborder="0" scrolling="no" width="728" height="90" marginwidth="0" marginheight="0"></iframe>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
    <!-- Header menu -->
    <div id="cs-header-menu">
        <div class="cs-container">
            <!-- Main navigation -->
            <div class="cs-toggle-main-navigation"><i class="fa fa-bars"></i></div>
            <nav id="cs-main-navigation" class="cs-clearfix">
                <ul class="cs-main-navigation cs-clearfix">
                    <li class="current-menu-item"><h2><a href="/" title="<?php echo $this->info->title; ?>"><span>Home</span></a></h2></li>
                    <?php if(!empty($this->listMenu)) foreach ($this->listMenu as $item):
                        $active = '';
                        //if($item['category_id'] == $this->category_id) $active = 'current-menu-item';
                        ?>
                        <li class="<?php echo $active; ?>"><h2><a href="<?php echo $this->getUrlCate($item->category_id); ?>" title="<?php echo $item->title_page; ?>"><?php echo $item->title; ?></a></h2>
                            <?php $listChild = $this->listChildCate($item->category_id); if(!empty($listChild)): ?>
                                <ul class="sub-menu">
                                    <?php foreach($listChild as $item1):  ?>
                                    <li class="current-menu-item"><h2><a href="<?php echo $this->getUrlCate($item1->category_id); ?>" title="<?php echo $item1->title_page; ?>"><?php echo $item1->title; ?></a></h2>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
            <!-- Search icon show -->
            <div id="cs-header-menu-search-button-show"><i class="fa fa-search"></i></div>
            <!-- Search icon -->
            <div id="cs-header-menu-search-form">
                <div id="cs-header-menu-search-button-hide"><i class="fa fa-close"></i></div>
                <input id="keyword" type="text" placeholder="Type and press enter..." class="input-group fr">
            </div>
        </div>
    </div>
</div>