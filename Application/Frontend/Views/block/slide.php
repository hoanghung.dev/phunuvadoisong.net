<div class="row">
    <div class="span8">
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
                <?php if(!empty($this->images))?>
                <div class="item active">
                    <img alt="" src="<?php echo _ROOT_IMAGE . $this->images[0]->file;?>">
                    <div class="carousel-caption">
                        <h4><?php echo $this->images[0]->title;?></h4>
                        <p><?php print $this->images[0]->intro;?></p>
                    </div>

                </div>
                <?php foreach($this->images as $image):?>
                    <?php if($image != $this->images[0]):?>
                        <div class="item">
                            <img alt="<?php echo $image->title;?>" src="<?php echo _ROOT_IMAGE . $image->file;?>">
                            <div class="carousel-caption">
                                <h4><?php echo $image->title;?></h4>
                                <p><?php print $image->intro;?></p>
                            </div>
                        </div>
                    <?php endif;?>
                <?php endforeach;?>
            </div>

            <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
        </div>
    </div>
</div>