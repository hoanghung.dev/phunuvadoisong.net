<!-- Main sidebar -->
<div class="cs-main-sidebar cs-sticky-sidebar">
    <aside class="wiget">
        <iframe src="http://ylx-2.com/banner_show_safe.php?section=General&amp;pub=791475&amp;format=300x250&amp;ga=g" frameborder="0" scrolling="no" width="300" height="250" marginwidth="0" marginheight="0"></iframe>
    </aside>

    <aside class="wiget">
        <?php print $this->GetAds('right');?>
    </aside>

    <aside class="widget">
        <h2 class="widget-title">Social icons</h2>

        <div class="cs-widget_social_icons">
            <!-- Facebook icon -->
            <div class="social-button facebook">
                <a class="facebook"
                   onClick="window.open('http://www.facebook.com/sharer.php?u=<?php echo _ROOT_HOME; ?>','Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                   href="http://www.facebook.com/sharer.php?u=<?php echo _ROOT_HOME; ?>">
                    <div class="social-icon"><i class="fa fa-facebook"></i></div>
                    <div class="social-wrap">
                        <div class="social-title">Facebook</div>
                        <div class="social-subtitle">678,356</div>
                    </div>
                </a>
            </div>
            <!-- Twitter icon -->
            <div class="social-button twitter">
                <a class="twitter"
                   onClick="window.open('http://twitter.com/share?url=<?php echo _ROOT_HOME; ?>&amp;text=<?php echo $this->info->title; ?>','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;"
                   href="http://twitter.com/share?url=<?php echo _ROOT_HOME; ?>&amp;text=<?php echo $this->info->title; ?>">
                    <div class="social-icon"><i class="fa fa-twitter"></i></div>
                    <div class="social-wrap">
                        <div class="social-title">Twitter</div>
                        <div class="social-subtitle">16,074</div>
                    </div>
                </a>
            </div>
            <!-- Google icon -->
            <div class="social-button google">
                <a class="googleplus"
                   onClick="window.open('https://plus.google.com/share?url=<?php echo _ROOT_HOME; ?>','Google plus','width=585,height=666,left='+(screen.availWidth/2-292)+',top='+(screen.availHeight/2-333)+''); return false;"
                   href="https://plus.google.com/share?url=<?php echo _ROOT_HOME; ?>">
                    <div class="social-icon"><i class="fa fa-google-plus"></i></div>
                    <div class="social-wrap">
                        <div class="social-title">Google+</div>
                        <div class="social-subtitle">140,580</div>
                    </div>
                </a>
            </div>
            <!--  icon -->
            <div class="social-button youtube">
                <a class="pinterest"
                   href='javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());'>
                    <div class="social-icon"><i class="fa fa-youtube-play"></i></div>
                    <div class="social-wrap">
                        <div class="social-title">Youtube</div>
                        <div class="social-subtitle">80,324</div>
                    </div>
                </a>
            </div>

            <!--<div class="fb-page" data-href="https://www.facebook.com/Buzz-24H-1729584200614991/" data-tabs="timeline"
                 data-height="130" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                 data-show-facepile="false"></div>-->
        </div>
    </aside>
    <aside class="widget">
        <h2 class="widget-title">Bài viết nổi bật</h2>

        <div class="cs-widget_featured_post">
            <?php if (!empty($this->listHot)) foreach ($this->listHot as $key => $oneItem): $oneCate = $this->getCategory($oneItem->category_id, 'title,title_page'); ?>
                <div class="cs-post-item">
                    <div class="cs-post-category-icon">
                        <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>"
                           title="<?php echo $oneCate->title_page; ?>"><i class="<?php echo $oneCate->class; ?>"></i>
                        </a>
                    </div>
                    <div class="cs-post-thumb">
                        <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                           title="<?php echo $oneItem->title; ?>"><img
                                src="<?php echo $this->getImageNews($oneItem->news_id, 450, 275); ?>"
                                alt="<?php echo $oneItem->title; ?>"></a>
                    </div>
                    <div class="cs-post-inner">
                        <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                               title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>

                        <div class="cs-post-meta cs-clearfix">
                            <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                            <span
                                class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time, 'd-m-Y'); ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </aside>
    <aside class="widget">
        <h2 class="widget-title">Bài viết mới nhất</h2>

        <div class="cs-widget_latest_posts">
            <?php if (!empty($this->listLastestNews)) foreach ($this->listLastestNews as $key => $oneItem): $oneCate = $this->getCategory($oneItem->category_id, 'title,title_page'); ?>
                <div class="cs-post-item">
                    <div class="cs-post-thumb">
                        <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                           title="<?php echo $oneItem->title; ?>"><img
                                src="<?php echo $this->getImageNews($oneItem->news_id, 100, 75); ?>"
                                alt="<?php echo $oneItem->title; ?>"></a>
                    </div>
                    <div class="cs-post-inner">
                        <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>"
                               title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>

                        <div class="cs-post-category-empty cs-clearfix">
                            <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>"
                               title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                        </div>
                        <div class="cs-post-meta cs-clearfix">
                            <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                            <span
                                class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time); ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </aside>
    <aside class="widget">
        <iframe src="http://ylx-1.com/banner_show.php?section=General&amp;pub=791475&amp;format=300x250&amp;ga=g" frameborder="0" scrolling="no" width="300" height="250" marginwidth="0" marginheight="0"></iframe>
    </aside>
</div>