<!-- Footer menu -->
<div id="cs-footer-menu">
    <div class="cs-container">
        <!-- Footer navigation -->
        <div class="cs-toggle-footer-navigation"><i class="fa fa-bars"></i></div>
        <nav id="cs-footer-navigation" class="cs-clearfix">
            <ul class="cs-footer-navigation cs-clearfix">
                <li><a href="/" title="<?php echo $this->info->title; ?>"><span>Home</span></a></li>
                <?php if(!empty($this->listMenu)) foreach ($this->listMenu as $item): ?>
                    <li><a href="<?php echo $this->getUrlCate($item->category_id); ?>" title="<?php echo $item->title_page; ?>"><?php echo $item->title; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
</div>
<!-- Copyirght -->
<div id="cs-copyright">
    <div class="cs-container">PhuNuVaDoiSong.Net &copy; Copyright 2016. All rights reserved.</div>
</div>