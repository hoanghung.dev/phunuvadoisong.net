<div class="row">
    <ul class="breadcrumb span8">
        <li><a href="<?php echo _ROOT_HOME;?>">Home</a> <span class="divider">/</span></li>
        <?php if(!empty($this->breadcrumb)):?>
            <?php foreach($this->breadcrumb as $breadcrumb):?>
                <li><a href="<?php echo $breadcrumb['slug']?>"><?php echo $breadcrumb['title']?></a> <span class="divider">/</span></li>
            <?php endforeach;?>
        <?php endif;?>
    </ul>
</div>