<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <!-- Meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="alternate" hreflang="vi" href="<?php echo _ROOT_HOME; ?>" />
<?php if(isset($this->seo)): $SEO = $this->seo; ?>
    <title><?php echo isset($SEO['title'])?$SEO['title']:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?></title>
    <meta name="description" content="<?php echo isset($SEO['description'])?strip_tags($SEO['description']):''; ?>"/>
    <meta name="keywords" content="<?php echo isset($SEO['keywords'])?$SEO['keywords']:''; ?>"/>
    <meta property="og:title" content="<?php echo isset($SEO['title'])?$SEO['title']:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?>" />
    <meta property="og:description" content="<?php echo isset($SEO['description'])?strip_tags($SEO['description']):''; ?>" />
    <meta property="og:image" content="<?php echo $this->getImageNews($SEO['image'],600,300); ?>" />
    <meta property="og:url" content="<?php echo isset($SEO['link'])?$SEO['link']:''; ?>" />
    <link rel="canonical" href="<?php echo isset($SEO['link'])?$SEO['link']:''; ?>"/>
<?php else: ?>
    <title><?php echo isset($this->info->title_page)?$this->info->title_page:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?></title>
    <meta name="description" content="<?php echo isset($this->info->description)?$this->info->description:''; ?>"/>
    <meta name="keywords" content="<?php echo isset($this->info->keywords)?$this->info->keywords:''; ?>"/>
    <meta property="og:title" content="<?php echo isset($this->info->title_page)?$this->info->title_page:''; ?> | <?php echo isset($this->info->title)?$this->info->title:''; ?>" />
    <meta property="og:description" content="<?php echo isset($this->info->description)?$this->info->description:''; ?>" />
    <meta property="og:image" content="<?php echo $this->getImageNews('logo.png',200,200); ?>" />
    <meta property="og:url" content="<?php echo _ROOT_HOME; ?>" />
    <link rel="canonical" href="<?php echo _ROOT_HOME; ?>"/>
<?php endif; ?>
    <meta property="og:type" content="website" />
    <meta name="author" content="buzz24h.com" />
    <meta property="og:site_name" content="<?php echo _ROOT_HOME; ?>" />
    <meta property="fb:admins" content="steven.mucian"/>
    <meta property="fb:app_id" content="624120221076245"/>
    <!-- Favicons -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/typography.css">
    <link rel="stylesheet" href="/css/fontawesome.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/colors.css">
    <link rel="stylesheet" href="/css/custom.css">
    <!-- Responsive -->
    <link rel="stylesheet" media="(max-width:767px)" href="/css/0-responsive.css">
    <link rel="stylesheet" media="(min-width:768px) and (max-width:1024px)" href="/css/768-responsive.css">
    <link rel="stylesheet" media="(min-width:1025px) and (max-width:1199px)" href="/css/1025-responsive.css">
    <link rel="stylesheet" media="(min-width:1200px)" href="/css/1200-responsive.css">
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81180350-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div id="fb-root"></div>
<div id="cs-wrapper" class="wide">
    <?php echo $this->action('boxHeader','block','Frontend',$this->params); ?>
    <?php echo $this->layoutContent; ?>
    <?php echo $this->action('boxFooter','block','Frontend',$this->params); ?>
</div>
<!-- Javascripts -->
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/js/jquery-easing.js"></script>
<script type="text/javascript" src="/js/jquery-fitvids.js"></script>
<script type="text/javascript" src="/js/jquery-sticky.js"></script>
<script type="text/javascript" src="/js/jquery-viewportchecker.js"></script>
<script type="text/javascript" src="/js/jquery-swiper.js"></script>
<script type="text/javascript" src="/js/jquery-magnific.js"></script>
<script type="text/javascript" src="/js/jquery-countdown.js"></script>
<script type="text/javascript" src="/js/jquery-init.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="//cdn.chitika.net/getads.js" async></script>
<!--<script src="//connect.facebook.net/en_US/all.js"></script>-->
<!--<script async defer src="//assets.pinterest.com/js/pinit.js"></script>-->
<script type="text/javascript">var SC_CId = "166851",SC_Domain="n.ads2-adnow.com";SC_Start_166851=(new Date).getTime();</script>
<script type="text/javascript" src="http://st-n.ads2-adnow.com/js/adv_out.js"></script>
<!--<script type="text/javascript" src="http://ylx-4.com/layer.php?section=General&pub=791475&ga=g&show=1&fp"></script>-->
<script>
    window.fbAsyncInit = function() {
        FB.Event.subscribe(
            'ad.loaded',
            function(placementId) {
                console.log('Audience Network ad loaded');
                document.getElementById('ad_root').style.display = 'block';
            }
        );
        FB.Event.subscribe(
            'ad.error',
            function(errorCode, errorMessage, placementId) {
                console.log('Audience Network error (' + errorCode + ') ' + errorMessage);
            }
        );
    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk/xfbml.ad.js#xfbml=1&version=v2.5&appId=624120221076245";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">
    <?php echo $this->info->script; ?>
</script>
</body>
</html>