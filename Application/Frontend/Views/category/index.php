<?php $oneCate = $this->oneCate; ?>
<div class="cs-container">
    <!-- Page title -->
    <h1 class="cs-page-title"><a href="<?php echo $this->getUrlCate($oneCate->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a></h1>
    <p><?php echo $oneCate->intro; ?></p>
    <!-- Main content -->
    <div class="cs-main-content cs-sidebar-on-the-right">
        <!-- Block layout 4 -->
        <div class="cs-post-block-layout-4">
            <?php if(!empty($this->listNews)) foreach($this->listNews as $oneItem): $category = $this->getCategory($oneItem->category_id,'title,title_page'); ?>
            <!-- Post item -->
            <div class="cs-post-item hidden visible cs-animate-element">
                <div class="cs-post-thumb">
                    <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id, 450, 275); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                </div>
                <div class="cs-post-inner">
                    <h2><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h2>
                    <div class="cs-post-meta cs-clearfix">
                        <span class="cs-post-category"><a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $category->title_page; ?>"><?php echo $category->title; ?></a> </span>
                        <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                        <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time); ?></span>
                                    <span class="cs-post-meta-rating" title="Rated 2.50 out of 5">
                                        <span style="width: 50%">2.50 out of 5</span>
                                    </span>
                    </div>
                    <p class="cs-post-excerpt"><?php echo $oneItem->intro; ?></p>
                    <a class="cs-post-read-more" href="<?php echo $this->getUrlNews($oneItem->news_id); ?>">Read more <i
                            class="fa fa-angle-double-right"></i></a>
                </div>
            </div>
            <?php endforeach;?>
        </div>
        <!-- Pagination -->
        <div class="text-center" >
            <ul class="page-numbers">
                <?php echo isset($this->page)?$this->page:''; ?>
            </ul>
        </div>
    </div>

    <?php echo $this->action('boxSidebar', 'Block', 'Frontend');?>
</div>