<?php header("Content-Type: text/xml;charset=iso-8859-1");
?>
<?xml version="1.0" encoding="UTF-8"?>
<urlset
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
	  		http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <url>
        <loc><?php echo _ROOT_HOME;?></loc>
        <priority>1.00</priority>
        <changefreq>Always</changefreq>
    </url>
    <?php
    $categoryModel = new Application\Frontend\Models\Categories();
    $listCategory = $categoryModel->getDataArr();?>
    <?php if(!empty($listCategory)) foreach ($listCategory as $item):?>
    <url>
        <loc><?php echo _ROOT_HOME . '/category/' . $item->slug .'-'. $item->category_id;?></loc>
        <priority>0.90</priority>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach;?>

    <?php
    $newsModel = new Application\Frontend\Models\News();
    $listNew = $newsModel->getDataArr(['limit' => 10]);?>
    <?php if(!empty($listNew)) foreach ($listNew as $item):?>
        <url>
            <loc><?php echo $this->getUrlNews($item->news_id);?></loc>
            <priority>0.20</priority>
            <changefreq>month</changefreq>
        </url>
    <?php endforeach;?>
</urlset>