<?php if(!empty($this->data)):  $oneCate = $this->oneCate; ?>
<div class="cs-row">
    <div class="cs-col cs-col-12-of-12">
        <div class="cs-post-block-title">
            <h3><a href="<?php echo $this->getUrlCate($oneCate->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"> <?php echo $oneCate->title; ?></a></h3>

            <?php
            $cateModel = new \Application\Frontend\Models\Categories();
            $childs = $cateModel->getDataArr(['parent_id' => $oneCate->category_id]);
            if (!empty($childs)):
                ?>
                <div class="fasion_right">
                    <ul>
                        <?php foreach ($childs as $child): ?>
                            <li>
                                <a href="<?php echo $this->getUrlCate($child->category_id); ?>"
                                   title="<?php echo $child->title_page; ?>"><?php echo $child->title; ?>
                                </a>
                            </li>
                        <?php endforeach;; ?>
                    </ul>
                </div>
                <?php
            endif;
            ?>
        </div>
    </div>
    <?php foreach ($this->data as $oneItem): $category = $this->getCategory($oneItem->category_id,'title,title_page'); ?>
        <div class="cs-col cs-col-6-of-12">
            <div class="cs-post-block-layout-3">
                <div class="cs-post-item">
                    <div class="cs-post-thumb">
                        <div class="cs-post-category-border cs-clearfix">
                            <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $category->title_page; ?>"><?php echo $category->title; ?></a>
                        </div>
                        <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id, 450, 275); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                    </div>
                    <div class="cs-post-inner">
                        <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                        <div class="cs-post-meta cs-clearfix">
                            <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                            <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time); ?></span>
                            <span class="cs-post-meta-rating" title="Rate News">
                                <span style="width: 70%">4.50 out of 5</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>