<div class="cs-container" style="transform: none;">
    <!-- Main content -->
    <div class="cs-main-content" style="transform: none;">
        <div style="text-align: center;font-size: 15pt;font-weight: bold;margin: 5px;"> Oops! Trang bạn cần tìm không có. Vui lòng thử lại trang khác !</div>
        <div class="cs-single-related-aticles">
            <h4 class="cs-heading-subtitle">Bài viết mới nhất</h4>

            <div class="cs-row">
                <?php if(!empty($this->listNews)) foreach($this->listNews as $oneItem): $oneCate = $this->getCategory($oneItem->category_id,'title,title_page'); ?>
                    <div class="cs-col cs-col-4-of-12">
                        <!-- Block layout 3 -->
                        <div class="cs-post-block-layout-3">
                            <!-- Post item -->
                            <div class="cs-post-item hidden visible cs-animate-element">
                                <div class="cs-post-thumb">
                                    <div class="cs-post-category-border cs-clearfix">
                                        <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"><?php echo $oneCate->title; ?></a>
                                    </div>
                                    <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id,215,150); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                                </div>
                                <div class="cs-post-inner">
                                    <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                                    <div class="cs-post-meta cs-clearfix">
                                        <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                        <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time,'d-m-Y'); ?></span>
                                                <span class="cs-post-meta-rating" title="Rated 3.50 out of 5">
                                                    <span style="width: 70%">3.50 out of 5</span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>