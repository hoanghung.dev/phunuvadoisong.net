<?php if($this->listHot) foreach ($this->listHot as $item): ?>
    <div class="cs-post-item cs-width-50">
        <div class="cs-post-thumb">
            <div class="cs-post-category-icon">
                <a href="<?php echo $this->getUrlNews($item->news_id);?>" title="<?php echo $item->title;?>"><i class="fa fa-newspaper-o"></i></a>
            </div>
            <a href="<?php echo $this->getUrlNews($item->news_id);?>"><img src="<?php echo $this->getImageNews($item->news_id,510,350); ?>" alt="<?php echo $item->title;?>"></a>
        </div>
        <div class="cs-post-inner">
            <h3><a href="<?php echo $this->getUrlNews($item->news_id);?>"><?php echo $item->title;?></a></h3>
            <div class="cs-post-meta cs-clearfix">
                <span class="cs-post-meta-date"><?php echo $this->timeAgo($item->created_time,'d m Y'); ?></span>
            </div>
        </div>
    </div>
<?php endforeach; ?>