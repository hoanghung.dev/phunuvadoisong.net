<?php if(!empty($this->data)):  $oneCate = $this->oneCate; ?>
<div class="cs-row">
    <div class="cs-col cs-col-12-of-12">
        <div class="cs-post-block-title">
            <h3><a href="<?php echo $this->getUrlCate($oneCate->category_id); ?>" title="<?php echo $oneCate->title_page; ?>"> <?php echo $oneCate->title; ?></a></h3>
            <!--<p><?php /*echo $oneCate->intro; */?></p>-->
        </div>
    </div>
    <?php foreach ($this->data as $key=>$oneItem): $category = $this->getCategory($oneItem->category_id,'title,title_page'); ?>
        <?php if($key == 0): ?>
            <div class="cs-col cs-col-6-of-12">
                <div class="cs-post-block-layout-3">
                    <div class="cs-post-item">
                        <div class="cs-post-thumb">
                            <div class="cs-post-category-border cs-clearfix">
                                <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $category->title_page; ?>"><?php echo $category->title; ?></a>
                            </div>
                            <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id, 417, 300); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                        </div>
                        <div class="cs-post-inner">
                            <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                            <div class="cs-post-meta cs-clearfix">
                                <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time); ?></span>
                            <span class="cs-post-meta-rating" title="Rate News">
                                <span style="width: 70%">4.50 out of 5</span>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="cs-col cs-col-6-of-12">
                <div class="cs-post-block-layout-2">
                    <div class="cs-post-item">
                        <div class="cs-post-thumb">
                            <a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><img src="<?php echo $this->getImageNews($oneItem->news_id, 100, 75); ?>" alt="<?php echo $oneItem->title; ?>"></a>
                        </div>
                        <div class="cs-post-inner">
                            <h3><a href="<?php echo $this->getUrlNews($oneItem->news_id); ?>" title="<?php echo $oneItem->title; ?>"><?php echo $oneItem->title; ?></a></h3>
                            <div class="cs-post-category-empty cs-clearfix">
                                <a href="<?php echo $this->getUrlCate($oneItem->category_id); ?>" title="<?php echo $category->title_page; ?>"><?php echo $category->title; ?></a>
                            </div>
                            <div class="cs-post-meta cs-clearfix">
                                <span class="cs-post-meta-author"><?php echo $this->getAuthor($oneItem->user_id); ?></span>
                                <span class="cs-post-meta-date"><?php echo $this->timeAgo($oneItem->created_time); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
<?php endif; ?>