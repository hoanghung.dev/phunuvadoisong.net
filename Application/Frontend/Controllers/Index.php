<?php
namespace Application\Frontend\Controllers;

use App\Models\Category;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;

class Index extends Base
{
    public function index(){
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $this->view->listCategory = $categoryModel->getDataArr($params);
        $this->displayLayout('default',$this->render());
    }
    public function listNews1(){
        $oneCate = (object)$this->_request->getParams();
        $listNews = $this->_helper->listNewsInCate($oneCate->category_id);
        $this->view->oneCate = $oneCate;
        $this->view->data = $listNews;
        echo $this->render();
    }
    public function listNews2(){
        $oneCate = (object)$this->_request->getParams();
        $listNews = $this->_helper->listNewsInCate($oneCate->category_id);
        $this->view->oneCate = $oneCate;
        $this->view->data = $listNews;
        echo $this->render();
    }
    public function boxHot(){
        $newsModel = new News();
        $params['is_hot'] = 1;
        $params['limit'] = 2;
        $this->view->listHot = $newsModel->getDataArr($params);
        echo $this->render();
    }
    public function boxMostViewed(){
        $newsModel = new News();

        $listHot = [];
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 15]);
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 30]);
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 19]);
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 21]);
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 27]);
        $listHot[] = $newsModel->getOne('category_id = :cat_id ORDER BY news_id DESC', [':cat_id' => 37]);

        $this->view->listMostViewed = $listHot;
        echo $this->render();
    }
    public function notFound(){
        $newsModel = new News();
        $params['select'] = 'news_id, category_id, title, created_time,user_id ';
        $params['order_by'] = "news_id DESC";
        $params['limit'] = 9;
        $this->view->listNews = $newsModel->getDataArr($params);
        $this->displayLayout('default',$this->render());
    }
    public function sitemap() {
        $categoryModel = new Categories();
        $this->view->listCategory = $categoryModel->getDataArr();
        echo $this->render();
    }

}