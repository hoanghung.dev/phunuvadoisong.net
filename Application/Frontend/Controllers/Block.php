<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 13/05/2014
 * Time: 15:13
 */
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\News;
use Application\Frontend\Models\Categories;

class Block extends Base
{
    public $arrBcrumb = array();

    public function boxHeader() {
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $params['order_by'] = 'category_id ASC';
        $this->view->listMenu = $categoryModel->getDataArr($params);
        echo $this->render();
    }
    function getCategoryId(){
        $params = $this->params();
        if($params['controller'] == 'category'){
            $id = $params['controller']['id'];
        }
    }

    public function breadcrumb(){
        $newsModel = new News();
        $params = $this->_request->getParams();
        $controller = $params['router']['controller'];
        $id = isset($params['id']) ? $params['id'] : '';
        $slug = isset($params['slug']) ? $params['slug'] : '';
        $mapArr = array();
        switch($controller){
            case 'article':
                $oneNews = $newsModel->getOne('news_id = ?',array($id),'news_id,category_id,title,slug');
                $this->recursionCategory($oneNews->category_id, 0);
                $mapArr[10]['title'] = $oneNews->title;
                $mapArr[10]['slug'] = $oneNews->slug . '-' . $oneNews->news_id . '.html';
                if(!empty($this->arrBcrumb)) foreach($this->arrBcrumb as $key => $value){
                    $mapArr[$key] = $value;
                }
                break;
           case 'category':
                $category_model = new Categories();
                $category = $category_model->getOne('slug = :slug', array(':slug'=>$slug),'category_id');
                $this->recursionCategory($category->category_id, 0);
                if(!empty($this->arrBcrumb)) foreach($this->arrBcrumb as $key => $value){
                   $mapArr[$key] = $value;
                }
                break;
        }
        $this->view->breadcrumb = array_reverse($mapArr);
        echo $this->render();
    }

    private function recursionCategory($cateId, $i){
        $categoryModel = new Categories();
        $dataCate = $categoryModel->getOne('category_id = :id',array(':id'=>$cateId),'parent_id,title,slug,category_id');
        if(isset($dataCate) && $dataCate != false){
            $this->arrBcrumb[$i]['title'] = $dataCate->title;
            $this->arrBcrumb[$i]['slug'] = _ROOT_HOME . $dataCate->slug;
            if($dataCate->parent_id != 0){
                $i++;
                $this->recursionCategory($dataCate->parent_id,$i);
            }
        }
        return true;
    }

    public function slide() {
        $image_model = new Image();
        $this->view->images = $image_model->getDataArr(array('type'=>2));
        echo $this->render();
    }

    public function search() {
        echo $this->render();
    }

    public function pageface() {
        echo $this->render();
    }

    public function boxSideBar() {
        $newsModel = new News();
        $params['select'] = 'news_id,title,category_id, intro,created_time,user_id';
        $params['order_by'] = 'news_id DESC';
        $params['limit'] = 6;
        $this->view->listLastestNews = $newsModel->getDataArr($params);
        $this->view->listAlbum = $newsModel->getDataArr($params);
        $params['order_by'] = 'viewed DESC';
        $params['limit'] = 2;
        $this->view->listHot = $newsModel->getDataArr($params);
        echo $this->render();
    }



    public function boxFooter(){
        $categoryModel = new Categories();
        $params['parent_id'] = '0';
        $this->view->listMenu = $categoryModel->getDataArr($params);
        echo $this->render();
    }


}