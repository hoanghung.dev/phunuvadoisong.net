<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;
use Application\Library\Pagination;

class Category extends Base
{

    public function index() {
        $categoryModel = new Categories();
        $newsModel = new News();
        $categoryId = $this->_request->getParam('id');
        $page = $this->_request->getParam('page',1);
        
        $data = $categoryModel->getOne('category_id = :id',array(':id'=>$categoryId));
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time';
        $params['category_in'] = $this->listChild($data->category_id);
        $params['order_by'] = 'created_time DESC';
        $params['page'] = $page;
        $params['limit'] = 20;

        $total = $newsModel->getCount($params);
        $this->view->oneCate = $data;
        $this->view->listNews = $newsModel->getDataArr($params);

        
        $this->view->page = $this->getPaging($total,$page,$params['limit'],5,$this->_helper->getUrlCate($data->category_id));

        $SEO['title'] = $data->title;
        $SEO['link'] = $this->_helper->getUrlCate($data->category_id);
        $SEO['description'] = $data->intro;
        $SEO['keywords'] = $data->keywords;
        //$SEO['image'] = $this->_helper->getImageCate($data->news_id);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }
    function listChild($id){
        $categoryModel = new Categories();
        $params['select'] = 'category_id';
        $params['parent_id'] = $id;
        $data = $categoryModel->getDataArr($params);
        $list = $id;
        if(!empty($data)) foreach ($data as $key=>$item){
            $list .= ','.$item->category_id;
        }
        return $list;
    }

}