<?php
namespace Application\Frontend\Controllers;

use Application\Frontend\Models\News;
use Application\Frontend\Models\Users;

class Article extends Base {

    public function search() {
        $newsModel = new News();
        $tag = $this->_request->getParam('q');
        $activePage = $this->_request->getParam('page',1);

        $this->view->url = $url = '/search/'.$tag;
        $this->view->tag = $title = urldecode($tag);
        $params['select'] = 'news_id, category_id, title,title_page, intro, user_id, created_time';
        $params['search'] = $title;
        $params['order_by'] = 'news_id DESC';
        $params['page'] = $activePage;
        $params['limit'] = 20;

        $total = $newsModel->getCount($params);

        $this->view->listNews = $newsModel->getDataArr($params);

        $this->view->page = $this->getPaging($total,$activePage,$params['limit'],5,$url);
        $SEO['title'] = "Search: ".$title;
        $SEO['link'] = $url;
        $SEO['description'] = "Search for ".$title .': This info '.$title.' video clip, image photo about '.$title;
        $SEO['keywords'] = $title;
        //$SEO['image'] = $this->_helper->getImageNews($);
        $this->view->seo = $SEO;
        $this->displayLayout('default',$this->render());
    }

    public function detail() {
        $news_id = $this->_request->getParam('id');
        $new_model = new News();

        $this->view->new = $new = $new_model->getOne('news_id = :news_id and status = 1', array(':news_id'=>$news_id),'*',3600*24);
        if(empty($new)) $this->redirect('/404.html');
        $params1['prev_id'] = $news_id;
        $params1['category_id'] = $new->category_id;
        $params1['limit'] = 1;
        $params1['order_by'] = 'news_id desc';
        $this->view->new_prev = $new_model->getDataArr($params1);

        $params2['next_id'] = $news_id;
        $params2['category_id'] = $new->category_id;
        $params2['limit'] = 1;
        $params2['order_by'] = 'news_id asc';
        $this->view->new_next = $new_model->getDataArr($params2);

        $SEO['title'] = $new->title;
        $SEO['link'] = $this->_helper->getUrlNews($news_id);
        $SEO['description'] = $new->intro;
        $SEO['keywords'] = $new->keywords;
        $SEO['image'] = $new->image;
        $this->view->seo = $SEO;
        $this->view->relate_category = $new_model->getDataArr(array('status'=>1,'category_id'=>$new->category_id,'order_by'=>'created_time DESC','limit'=>3));
        $this->view->relate_tag = $new_model->getDataArr(array('status'=>1,'search'=>$new->title,'order_by'=>'created_time DESC','limit'=>3));
        $this->displayLayout('default',$this->render());
    }

}