<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 23/04/2015
 * Time: 09:15 CH
 */
namespace Application\Frontend\Models;

use Soul\Mvc\Model;
use Soul\Registry;

class Tags extends Model
{
    protected  $_tbl ='default_tags';

    public function init()
    {
        $this->_mysql = Registry::get('Mysql');
        $this->_memcache = Registry::get('Memcache');
    }

    public function getAll()
    {
        $sql = 'SELECT * FROM ' . $this->_tbl;
        $st = $this->_mysql->prepare($sql);
        $st->execute();
        $data = $st->fetchAll(\PDO::FETCH_OBJ);
        return $data;
    }

    public function getOne($where, $bind, $select="*")
    {
        $sql = sprintf('SELECT %s FROM %s WHERE %s',$select,$this->_tbl, $where);
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));

        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else {
            $st = $this->_mysql->prepare($sql);
            $st->execute($bind);
            $data = $st->fetch(\PDO::FETCH_OBJ);
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }
    public function getCount($args = null)
    {
        $news_id = '';
        $category_id = '';
        $is_home = '';
        $is_trash = '';
        $not_in = '';
        $in = '';
        $search = '';
        $bind = array();


        $default = array('news_id' => 0,'category_id'=>NULL,'is_trash'=>0,'is_home'=>NULL,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE is_trash = :is_trash';
        $bind[] = array(
            'element'=>':is_trash',
            'value'=>$is_trash,
            //'type'=>'\PDO::PARAM_INT'
        );

        if ($news_id != 0){
            $where .= ' AND news_id = :news_id';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$news_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($is_home != NULL){
            $where .= ' AND is_home = :is_home';
            $bind[] = array(
                'element'=>':is_home',
                'value'=>$is_home,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($category_id != NULL){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND title LIKE :search';
            $bind[] = array(
                'element'=>':search',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        $sql = sprintf('SELECT count(1) FROM %s %s', $this->_tbl, $where);
        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else{
            $st = $this->_mysql->prepare($sql);
            if(is_array($bind)) foreach($bind as $item){
                $st->bindParam($item['element'],$item['value'],isset($item['type'])?$item['type']:null,10);
            }
            $st->execute();
            $data = $st->fetchColumn();
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }
    public function getDataArr($args = null)
    {
        $select = '';
        $news_id = '';
        $category_id = '';
        $is_home = '';
        $is_trash = '';
        $not_in = '';
        $in = '';
        $order_by = '';
        $order = '';
        $limit = '';
        $page = '';
        $search = '';
        $bind = array();


        $default = array('select'=>'*','news_id' => 0,'category_id'=>NULL,'is_trash'=>0,'is_home'=>NULL,'not_in' => 0, 'in' => 0,'search'=> null, 'order_by' => null,'limit' => 0, 'page'=>1);
        $args = $this->parseArgs($args, $default);
        extract($args);

        $where = 'WHERE 1 = 1';
//        $bind[] = array(
//            'element'=>':is_trash',
//            'value'=>$is_trash,
//            //'type'=>'\PDO::PARAM_INT'
//        );


        if ($news_id != 0){
            $where .= ' AND news_id = :news_id';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$news_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($in != 0){
            $where .= ' AND news_id IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if ($not_in != 0){
            $where .= ' AND news_id NOT IN (:news_id)';
            $bind[] = array(
                'element'=>':news_id',
                'value'=>$not_in,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($is_home != NULL){
            $where .= ' AND is_home = :is_home';
            $bind[] = array(
                'element'=>':is_home',
                'value'=>$is_home,
                //'type'=>'\PDO::PARAM_INT'
            );
        }
        if ($category_id != NULL){
            $where .= ' AND category_id = :category_id';
            $bind[] = array(
                'element'=>':category_id',
                'value'=>$category_id,
                //'type'=>'\PDO::PARAM_INT'
            );
        }

        if($search != null){
            //$search = rawurldecode($search);
            $where .= ' AND title LIKE :search';
            $bind[] = array(
                'element'=>':search',
                'value'=>'%'.$search.'%',
                //'type'=>'\PDO::PARAM_STR'
            );
        }

        if($order_by != null)
            $order = sprintf('ORDER BY %s', $order_by);

        if($limit != 0){
            $page = intval($page);
            $offset = ($page-1)*$limit;
            $limit = sprintf('LIMIT %d,%d',$offset,$limit);
        }else $limit = 'LIMIT 0,10';

        $sql = sprintf('SELECT %s FROM %s %s %s %s',$select, $this->_tbl, $where, $order, $limit);
        //echo $sql; die();
        /* echo "<!--".$sql."-->";
         echo "<!--<pre>";
         print_r($bind);
         echo "</pre>-->";*/
        $key = md5(MEMCACHE_KEY.$sql.serialize($bind));
        if(ENABLE_MEMCACHED == true) $data = $this->_memcache->get($key);
        else $data = '';
        if(!empty($data)){
            return $data;
        }else {
            $st = $this->_mysql->prepare($sql);
            if (is_array($bind)) foreach ($bind as $item) {
                $st->bindParam($item['element'], $item['value'], isset($item['type']) ? $item['type'] : null, 10);
            }
            $st->execute();
            $data = $st->fetchAll(\PDO::FETCH_OBJ);
            if(ENABLE_MEMCACHED == true) $this->_memcache->set($key,$data,false,MEMCACHED_TIME);
            return $data;
        }
    }

    public function insert($arr){
        return $this->_mysql->insert($this->_tbl, $arr);
    }
    public function update($arr, $where, $bind){
        return $this->_mysql->update($this->_tbl, $arr, $where, $bind);
    }
    public function delete($where,$bind){
        return $this->_mysql->delete($this->_tbl, $where, $bind);
    }
}