<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 26/04/2015
 * Time: 10:36 CH
 */
namespace Application\Frontend\Helpers;

use Application\Frontend\Models\Products;

class GetImageProduct{
    public function getImageProduct($id,$album = false){
        $productModel = new Products();
        if($album == false){
            $data = $productModel->getOne('product_id = ?',array($id),'image');
            return _ROOT_HOME.'/upload/'.$data->image;
        }else{
            $data = $productModel->getOne('product_id = ?',array($id),'image_album');
            if($data->image_album){
                $arr = explode('|',$data->image_album);
                $src = array();
                foreach($arr as $item){
                    $src[] = _ROOT_HOME.'/upload/'.$item;
                }
                return $src;
            }
        }
    }
}