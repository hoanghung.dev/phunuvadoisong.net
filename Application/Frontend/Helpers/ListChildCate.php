<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\Categories;

class ListChildCate extends Base{
    public function listChildCate($id){
        $categoryModel = new Categories();
        return $categoryModel->getDataArr(array('parent_id'=>$id));
    }
}