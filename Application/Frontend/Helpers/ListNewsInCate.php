<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Categories;
use Application\Frontend\Models\News;

class ListNewsInCate{
    public function listNewsInCate($cateId){
        $newsModel = new News();
        $params['select'] = 'news_id,title,category_id, created_time,intro,user_id ';
        $params['category_in'] = $this->listChild($cateId);
        $params['order_by'] = 'created_time DESC';
        $params['limit'] = 4;
        $data = $newsModel->getDataArr($params);
        return $data;
    }
    function listChild($id){
        $categoryModel = new Categories();
        $params['select'] = 'category_id';
        $params['parent_id'] = $id;
        $data = $categoryModel->getDataArr($params);
        $list = $id;
        if(!empty($data)) foreach ($data as $key=>$item){
            $list .= ','.$item->category_id;
        }
        return $list;
    }
}