<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 07/03/2014
 * Time: 22:57
 */
namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Advertises;

class getAds
{
    public function getAds($position) {
        $advertisesModel = new Advertises();

        $listItem = $advertisesModel->getDataArr(['position'=>$position, 'status'=>1]);

        if(count($listItem) > 0) {
            $str = '';
            foreach ($listItem as $item) {
                $str .= $item->content;
            }
            return $str;
        } else return '';
    }
}