<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Models\Categories;

class GetCateChild{
    public function getCateChild($id,$select='title,title_page'){
        $categoryModel = new Categories();
        return $categoryModel->getOne('parent_id = ?',array($id),$select);
    }
}