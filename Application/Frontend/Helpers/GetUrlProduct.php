<?php
/**
 * Created by PhpStorm.
 * User: ductoan1991
 * Date: 24/04/2015
 * Time: 09:46 SA
 */

namespace Application\Frontend\Helpers;
use Application\Frontend\Controllers\Base;
use Application\Frontend\Models\Products;

class GetUrlProduct extends Base{
    public function getUrlProduct($id){
        $productModel = new Products();
        $data = $productModel->getOne('product_id = ?',array($id),'slug,title');
        $slug = $this->toSlug($data->title);
        if($slug == null){
            $slug = $this->toSlug($data->title);
            $productModel->update(array('slug'=>$slug),'product_id = :id',array(':id'=>$id));
        }
        return _ROOT_HOME.'/san-pham/'.$slug.'-'.$id.'.html';
    }
}